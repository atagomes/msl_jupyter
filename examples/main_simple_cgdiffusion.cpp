#include "ApproxSpace/local_space_lagrange.h"
#include "ApproxSpace/local_space_piecewiselagrange.h"
#include "ApproxSpace/space_stdfiniteelem.h"

#include "Assemble/local_contrib_set.h"

#include "Block/block_galerkinlagrange.h"
#include "Block/block_onefunction.h"
#include "Block/weak_form_expression.h"

#include "Data/data_constant.h"
#include "Data/data_function.h"

#include "LinearSystem/linear_system_loc.h"

#include "Geometry/mesh.h"

#include "Utils/utils.h"

//INITIALIZE_EASYLOGGINGPP

int main(int argc, char *argv[]) {

    MSL_configure_easyloggingpp();

    std::string path_mesh(argv[1]); std::string name_mesh(argv[2]);
    int index_mesh = atoi(argv[3]);
    int dim = atoi(argv[4]); int iPk = atoi(argv[5]);

    //! [Loading and building a mesh]
    mesh_reader::MeshComponents will_read = mesh_reader::NODE|mesh_reader::ELEM;
    if (iPk > 1) { 
        will_read |= mesh_reader::EDGE; 
        if (dim == 2) will_read |= mesh_reader::NEIG; 
    }
    if (iPk > 2 && dim == 3) 
        will_read |= mesh_reader::FACE|mesh_reader::NEIG;

    Mesh mMesh;
    mMesh.load(path_mesh.c_str(),name_mesh.c_str(),index_mesh,1,will_read);

    mMesh.invert();                                  //builds inverse conn info
    if (iPk > 1) {
        if (dim == 2) mMesh.identifyEdges2D();
        else mMesh.identifyEdges3D();
    }
    if (iPk > 2 && dim == 3) mMesh.identifyFaces3D();
    mMesh.deleteInverse();                           //deletes inverse conn info
    //! [Loading and building a mesh]

    //! [Building local and global approximation spaces]
    LagrangeSpace localSpaceU;                       //local space is of SCALAR_TYPE
    localSpaceU.setElemInfo(
        mMesh.getMapOfElemInfoTypes().cbegin()->second);    //type of geometrical elem
    localSpaceU.setOrder(iPk);                       //order of the polynomials
    localSpaceU.defineDoFInfo();                     //builds local DoF info
    StdFiniteElementSpace spaceU(mMesh, localSpaceU);//builds global DoF info

    PiecewiseLagrangeSpace localSpaceU0;
    localSpaceU0.setElemInfo( mMesh.getMapOfElemInfoTypes().begin()->second);
    localSpaceU0.setOrder(0);
    localSpaceU0.defineDoFInfo();
    StdFiniteElementSpace spaceU0(mMesh, localSpaceU0);
    //! [Building local and global approximation spaces]

    //! [Building variational form]
    int f_Rk = iPk + 4; int Kappa_Rk = 2*(iPk-1) + 4;

    GalerkinLagrangeBlock phi(                       //for (f,phi)
        &spaceU, block::INNER_NODE, f_Rk, approx_space::U);
    GalerkinLagrangeBlock dphi(                      //for (grad_phi,grad_phi)
        &spaceU, block::INNER_NODE, Kappa_Rk, approx_space::gradU);
    FunctionData f;                                  //for (f,phi)
    f.createFunction("f","8*pi^2*cos(2*pi*x)*cos(2*pi*y)",
                     VECTOR2D_TYPE,                  //domain type for f
                     0.0,                            //time instant (N/A here);
                     SCALAR_TYPE);                   //codomain/image type for f

    LocalContribSet mLHS, mRHS;
    mLHS = ConstantData(1.0)*dphi*dphi;
    mRHS = DataBlock(f)*phi;
    //! [Building variational form]

    //! [Adding Lagrange multipliers]
    GalerkinLagrangeBlock multiplierLHS(
        &spaceU0, block::INNER_NODE, f_Rk, approx_space::U);
    mLHS += multiplierLHS*phi;
    mLHS += phi*multiplierLHS;

    GalerkinLagrangeBlock multiplierRHS(
        &spaceU0, block::INNER_NODE, 2, approx_space::U);
    ConstantData kernel(0.0);                        //imposing null average
    mRHS += DataBlock(kernel)*multiplierRHS;
    //! [Adding Lagrange multipliers]

    //! [Assembling and solving linear system]
    LocalLinearSystem Axb("");
    Axb.mountAxb(mLHS, mRHS);                        //assemble and factorize
    Axb.solveAxb();                                  //solve
    auto Tf = Axb.extractx();                        //gets x from A^{-1}b
    //! [Assembling and solving linear system]

    //! [Printing mesh and appending result]
    std::string path_output_mesh(argv[6]); std::string name_output_mesh(argv[7]);
    mMesh.print(path_output_mesh.c_str(), name_output_mesh.c_str());
    mMesh.appendResultHeader(); mMesh.appendResult(Vector(Tf));
    //! [Printing mesh and appending result]

    //! [Computing errors]
    FunctionData u;
    u.createFunction("u", "cos(2*pi*x)*cos(2*pi*y)",
                     VECTOR2D_TYPE,
                     0.0,
                     SCALAR_TYPE);

    int u_Rk = f_Rk;

    OneFunctionBlock errorBlock(Vector(Tf),
                                &spaceU,
                                spaceU.getTensorType(approx_space::U));
    errorBlock.setVarType(approx_space::U);
    errorBlock.create(block::INNER_NODE, u_Rk);

    double error = sqrt((errorBlock-u)*(errorBlock-u));
    LOG(INFO) << "L2-norm error: " << error << std::endl;
    //! [Computing errors]

    return 0;
}

