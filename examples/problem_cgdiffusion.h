/*!
 * @file problem_cgdiffusion.h
 *
 * This module implements the DiffusionCGProblem class, which illustrates the
 * specialization of the façade Problem class for solving \f$ (\nabla u, \nabla v) = (f,v) \f$
 *
 * @author Diego Paredes
 * @author Antonio Tadeu Gomes
 * @author Weslley Pereira
 *
 * @copyright Copyright 2013-2021 Laboratorio Nacional de Computacao Cientifica (LNCC)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the 
 * Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the 
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 ******************************************************************************/

#ifndef __FACADE__PROBLEMCGDIFFUSION_H__
#define __FACADE__PROBLEMCGDIFFUSION_H__

//Project configuration file

#ifndef __MSL_CG_CONFIG_H__
#include "msl_cg_config.h"
#endif

//C system header files

//C++ system header files

//Other libraries header files

//Project header files

#ifndef __APPROXSPACE_LOCALSPACELAGRANGE_H__
#include "ApproxSpace/local_space_lagrange.h"
#endif

#ifndef __APPROXSPACE_LOCALSPACEPIECEWISELAGRANGE_H__
#include "ApproxSpace/local_space_piecewiselagrange.h"
#endif

#ifndef __FACADE_PROBLEM_H__
#include "Facade/problem.h"
#endif

#ifndef __UTILS_UTILS_H__
#include "Utils/utils.h"
#endif

#ifdef USE_MPI
#ifdef USE_MUMPS
#ifndef __LINEARSYSTEM_LINEARSYSTEMDISTMUMPS_H__
#include "LinearSystem/linear_system_distmumps.h"
#endif
#endif
#endif

#ifndef __LINEARSYSTEM_LINEARSYSTEMLOC_H__
#include "LinearSystem/linear_system_loc.h"
#endif

//Forward declarations
class DiffusionCGProblem;
class ConstantData;
class DiracData;
class GaussianData;
class DecomposedSpaceTimeData;
class FunctionData;


//------------------------------------------------------------------------------
/*!
 * @brief This struct defines the regime trait for the diffusion problem as stationary.
 */
template<>
struct RegimeTrait<DiffusionCGProblem> {
    static RegimeType::RegimeT const regime = RegimeType::STATIONARY;
};

//------------------------------------------------------------------------------
/*!
 * @brief This struct defines the parallelism trait for the diffusion problem as distributed.
 */
template<>
struct ParallelismTrait<DiffusionCGProblem> {
    static ParallelismType::ParallelismT const parallelism = ParallelismType::DISTRIBUTED;
};

//------------------------------------------------------------------------------
/*!
 * @brief This struct defines the space order quantity type trait for diffusion problem as scalar.
 */
template<>
struct SpaceOrderTrait<DiffusionCGProblem> {
    static QuantityType::QuantityT const space_order = QuantityType::SCALAR_QUANTITY;
};

//------------------------------------------------------------------------------
/*!
 * @brief This struct defines a Data typelist for diffusion problem
 */
using DiffusionDataTypeList = DataTypeList<
ConstantData,
FunctionData/*,
DiracData,
GaussianData,
DecomposedSpaceTimeData*/
>;

//------------------------------------------------------------------------------
/*!
 * @brief This class solves the diffusion problem using the Continuous Galerkin Method
 *
 * This class solves the variation form (\f$ (\Kappa \nabla u, \nabla v) = (f,v) \f$)
 */
class DiffusionCGProblem: public Problem<DiffusionCGProblem> {

public:
    static const std::string name;

protected:

    std::shared_ptr<Data> kappa, f, u, du, dual_u;

    // Integration order
    int iPf;
    int iPkappa;
    int iPu;
    int iPdu;
    int iPdual_u;
    
    LagrangeSpace localSpaceU;
    PiecewiseLagrangeSpace localSpaceU0;

    MSL_MATRIX_FP_TYPE mSolution;
    
    std::shared_ptr<LinearSystem> mAXB;


public:

    /**************************************************************************
     * @functiongroup Constructors
     **************************************************************************/

    DiffusionCGProblem(const std::shared_ptr<mesh_reader::MeshReader> &aMeshReader =
                       std::make_shared<mesh_reader::TriTetMeshReader>());
    
    /**************************************************************************
     * @functiongroup Memory-related methods
     **************************************************************************/

    void cleanAXBImpl() {
	mAXB.reset();
    }

    /**************************************************************************
     * @functiongroup CRTP methods
     **************************************************************************/

    void configureSpacesImpl();

    void buildVariationalFormImpl();

    int getMaxKernelSizeImpl(int aDim) {
        return 1;
    }

    int getKernelPolynomialOrderForIntegrationImpl() {
        return 0;
    }

    void genKernelSizeImpl() {
            mDimKernel = (getDirichletLabels().size() == 0) ? 1 : 0;
    }
    
    std::shared_ptr<LocalSpace> genLocalKernelSpaceImpl() const {
        if (mDimKernel > 0)
            //TODO[Tadeu]: use of getRefElemInfo() here is not generic
            return std::make_shared<LagrangeSpace>(
                                       0,
                                       mMesh.getMapOfElemInfoTypes().begin()->second,
                                       SCALAR_TYPE);
        else
            return std::shared_ptr<LocalSpace>();
    }
    
    /*!
     * @brief Reads the file containing the problem data
     *
     * The file is a Lua table of the general form (some fields may be omitted
     * and receive default values):
     * \verbatim
     * diffusion = {
     *     kappa = ...,
     *     f     = ...,
     *     u     = ...,
     *     du    = ...,
     *     q     = ...,
     *
     *     kappa_iOrder = ...,
     *     f_iOrder     = ...,
     *     u_iOrder     = ...,
     *     du_iOrder    = ...,
     *     q_iOrder     = ...,
     *
     *     g = { ... },
     *     h = { ... }
     * }
     * \endverbatim
     */
    void readDataFilesImpl(const VectorOfStrings& df);
    
    const std::string& getNameImpl() const { return name; }

    /*!
     * @brief Read specific keys of a problem
     *
     * For this class no additional keys are provided
     *
     * @param aLuaScript Reference to the Lua configuration reader
     * @param aKey Key to be read
     */
    void readKeyImpl(
            const std::shared_ptr<LuaScript>& aLuaScript,
            const std::string &aKey) {}

    void solveImpl(bool aCleanLCS=true,
                   bool aCleanAXB=true);

    void solveImpl(const MSL_MATRIX_FP_TYPE& aRHS,
                   bool aCleanLCS=true,
                   bool aCleanAXB=true);

    /*!
     * @brief Returns the solution to the problem
     *
     * For this class a single solution is obtained (the scalar value of the
     * variable)
     */
    const MSL_MATRIX_FP_TYPE& getSolutionImpl() const { return mSolution; }

    /*!
     * @brief Returns the solution to the problem
     *
     * For this class a single solution is obtained (the scalar value of the
     * variable)
     */
    MSL_MATRIX_FP_TYPE& getSolutionImpl() { return mSolution; }
    
    /*!
     * @brief Returns the load vector (RHS in matricial form) of the problem
     *
     * For this class a single load vector is obtained: the nodal value of
     * f in (f,v).
     *
     * @pre The linear system must not have been released, otherwise
     * an error will be produced.
     */
    const Eigen::SparseMatrix<MSL_FP_TYPE>& getLoadVectorImpl() const {
        assert(mAXB);
        return mAXB->getSparseB();
    }

    /*!
     * @brief Sets the solution to the problem
     *
     * For this class a single solution is obtained (the scalar value of the
     * variable)
     *
     * @param[in] solution Solution to the pos-th variable
     */
    void setSolutionImpl(const MSL_MATRIX_FP_TYPE& solution) { mSolution = solution; }

    void printMeshWithHeadersImpl(
            const std::string& aPathOutput,
            const std::string& aOutputMeshName,
            bool binary = false) const {

        //Only variable in CG Diffusion is the scalar value of the variable
        mMesh.appendResultHeader(1.0,                        //delta_t
                                 0,                          //num_timesteps
                                 1,                          //increment
                                 0,                          //vecOrder=SCALAR
                                 0);                         //type=Node

    }

    void appendResultsToMeshImpl() const {

        //To print a solution we must account for the fact that the
        //partitioning strategy might have changed the DoF numbering
        //so that their indexes do not correspond to the indexes of the
        //geometric entities (vertices, elements etc), and we must recover
        //such correspondence before printing!
        MSL_VECTOR_FP_TYPE sol(mSolution.rows()-mDimKernel);
        if (mPartitionInfo == NULL)
            sol = mSolution.block(0, 0, mSolution.rows()-mDimKernel, 1);
        else {
            auto it_beg = mPartitionInfo->total_dof_begin();
            auto it_end = mPartitionInfo->total_dof_end();
            for (auto it = it_beg; it != it_end; ++it) {
                if (*it<mSolution.rows()-mDimKernel)
                    sol[*it] = mSolution(std::distance(it_beg, it), 0);
            }
        }
        mMesh.appendResult(Vector(sol));

    }

    std::shared_ptr<Data> getSourceFunctionImpl() const { return f; }
    int getIntegrationOrderForSourceImpl() const { return iPf; }

    void setLinearSystem(const std::shared_ptr<LinearSystem> &aAXB) { mAXB = aAXB; }
};

#endif /* __FACADE__PROBLEMCGDIFFUSION_H__ */
