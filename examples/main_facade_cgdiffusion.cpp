#include <cmath>

#include "problem_cgdiffusion.h"

#include "LinearSystem/linear_system.h"

//INITIALIZE_EASYLOGGINGPP

int main(int argc, char *argv[]) {

    MSL_configure_easyloggingpp();

    std::string path_mesh(argv[1]); std::string name_mesh(argv[2]);
    int index_mesh = atoi(argv[3]);
    int iPk = atoi(argv[4]);
    std::string path_data(argv[5]); std::string data_file(argv[6]);
    std::string path_output_mesh(argv[7]);

    //! [Creating a problem]
    std::shared_ptr<DiffusionCGProblem> problem =
            std::make_shared<DiffusionCGProblem>();

    problem->setPkList({iPk});

    problem->setPathMesh(path_mesh);
    problem->setNameMesh(name_mesh);
    problem->setIndexMesh(index_mesh);
    mesh_reader::MeshComponents what_to_read = mesh_reader::NODE|mesh_reader::ELEM;
    if (iPk > 1) { 
        what_to_read |= mesh_reader::EDGE;
        if (problem->getMeshDim() == 2)
            what_to_read |= mesh_reader::NEIG;
    }
    if (iPk > 2 && problem->getMeshDim() == 3)
        what_to_read |= mesh_reader::FACE|mesh_reader::NEIG;
    problem->buildMesh(what_to_read);

    problem->setPathData(path_data);
    problem->readDataFiles({data_file});
    problem->printData();

    problem->configureSpaces();
    problem->buildVariationalForm();
    //! [Creating a problem]

    //! [Solving a problem]
    problem->solve();
    //! [Solving a problem]

    //! [Printing a problem solution]
    problem->printMeshWithHeaders(
                path_output_mesh,
                "facade_output");
    problem->appendResultsToMesh();
    //! [Printing a problem solution]

    //! [Computing problem errors]
    MSL_FP_TYPE eL2 = problem->computeError("L2");
    MSL_FP_TYPE eSemiH1 = problem->computeError("semiH1");
    LOG(INFO) << "L2-norm error: " << eL2 << std::endl;
    LOG(INFO) << "H1-norm error: " << sqrt(eL2*eL2+eSemiH1*eSemiH1) << std::endl;
    //! [Computing problem errors]

    return 0;
}

