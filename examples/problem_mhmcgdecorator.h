/*!
 * @file problem_mhmcgdecorator.h
 *
 * This file contains an example of class definition for local mhm problems
 * decorators, which adds to the MHMLocalProblem façade class the capability
 * of building the lambda and kernel RHSs needed by the MHM method
 *
 * @author Diego Paredes
 * @author Antonio Tadeu Gomes
 * @author Weslley Pereira
 *
 * @copyright Copyright 2013-2021 Laboratorio Nacional de Computacao Cientifica (LNCC)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the 
 * Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the 
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 ******************************************************************************/

#ifndef __FACADE_PROBLEMMHMCGDECORATOR_H__
#define __FACADE_PROBLEMMHMCGDECORATOR_H__

//Project configuration file

#ifndef __MSL_MHM_CONFIG_H__
#include "msl_mhm_config.h"
#endif

//C system header files

//C++ system header files

//Other libraries header files

//Project header files

#ifndef __APPROXSPACE_SPACEFINITEELEM_H__
#include "ApproxSpace/space_finiteelem.h"
#endif

#ifndef __APPROXSPACE_SPACESTDFINITEELEM_H__
#include "ApproxSpace/space_stdfiniteelem.h"
#endif

#ifndef __APPROXSPACE_SPACEMHM_H__
#include "ApproxSpace/space_mhm.h"
#endif

#ifndef __BLOCK_BLOCKGALERKINLAGRANGE_H__
#include "Block/block_galerkinlagrange.h"
#endif

#ifndef __DATA_DATACONSTANT_H__
#include "Data/data_constant.h"
#endif

#ifndef __DATA_DATALAMBDA_H__
#include "Data/data_lambda.h"
#endif

#ifndef __FACADE_PROBLEMMHMLOCAL_H__
#include "Facade/problem_mhmlocal.h"
#endif

#ifndef __GEOMETRY_ELEMENT_H__
#include "Geometry/element.h"
#endif

//Forward declarations

//------------------------------------------------------------------------------
/*!
 * @brief This class provides a decorator implementation for a MHM local problem
 */

template<typename DecoratedProblem>
class CGMHMDecorator {
    
    int mLambdaTrialDim; //!< Size of Lambda space

    std::shared_ptr<MHMSpace> mLambdaSpace; //!< Space for Lambda RHS (etal's)

    /*
     * Used when DecoratedProblem has no kernel
     */
    std::shared_ptr<LocalSpace> mLocalSpaceU0; //!< Local Kernel basis
    std::shared_ptr<StdFiniteElementSpace> mSpaceU0; //!< Kernel space

    /*
     * Used when DecoratedProblem uses NonUniquenessTreatment::FIXDOFS
     */
    Eigen::SparseMatrix<MSL_FP_TYPE> lambdaTimesKernel, sourceTimesKernel; //!< Contributions for the global problem
    MSL_MATRIX_FP_TYPE mRHS; //!< matrix for the kernel RHS of the local problem
    
protected:
    using DecoratedMHMLocalProblem = MHMLocalProblem<DecoratedProblem, CGMHMDecorator<DecoratedProblem>>;
    DecoratedMHMLocalProblem *mDecoratedPtr;
    
public:
    
    /*************************************************************************
     * @functiongroup Constructors
     *************************************************************************/
    
    CGMHMDecorator(DecoratedMHMLocalProblem *aDecoratedPtr)
    : mLambdaSpace(nullptr)
    , mSpaceU0(nullptr)
    , mDecoratedPtr(aDecoratedPtr) {}

    /*************************************************************************
     * @functiongroup Getters and setters
     *************************************************************************/
    
    int getLambdaTrialDim() const { return mLambdaTrialDim; }

    const MSL_MATRIX_FP_TYPE& getRHS() const { return mRHS; }
    
    /*************************************************************************
     * @functiongroup Regular methods
     *************************************************************************/
    
    void buildMesh(mesh_reader::MeshComponents aMeshComps=mesh_reader::ALL);

    void configureLambdaSpace();
    
    void createLambdaRHS();
    
    void createKernelRHS();

    /*!
     * Here we multiply the transpose of the solution to the local global
     * with its right-hand side, so as to obtain the necessary inner products
     * that will make up contributions the local problem, as shown below:
     *  | etaf^t |   |   -   -   |   |          - etaf^t*l -          |
     *  | ------ |   |   -   -   |   | ------------------------------ |
     *  | etal^t | . | f - l - k | = |          - etal^t*l - etal^t*k |
     *  | ------ |   |   -   -   |   | ------------------------------ |
     *  | etak^t |   |   -   -   |   | etak^t*f - etak^t*l -          |
     *
     * Mind that here we could have used Eigen::BlockXpr again; but in this
     * case the performance would considerably decay because the multiplication
     * would be done several times (one for each part of the LHS and RHS matrices).
     */
    std::vector<double> genContribSourceTimesKernel();
    std::vector<double> genContribLambdaTimesKernel();
    std::vector<double> genContribLambdaTimesEtaL();
    std::vector<double> genContribLambdaTimesEtaf();

    void cleanContribs() {
        lambdaTimesKernel = Eigen::SparseMatrix<MSL_FP_TYPE>();
        sourceTimesKernel = Eigen::SparseMatrix<MSL_FP_TYPE>();
        mRHS = MSL_MATRIX_FP_TYPE();
    }

    /*!
     * It's VERY IMPORTANT to bear in mind that mEtal/f/k are all
     * of type Eigen::BlockXpr, which means they do not store the actual
     * data; rather, they are a view of 'X', which is in fact the set
     * of vectors representing the solution to the local problem!!!
     * Therefore, any changes in any of mEtal/f/k will also change
     * the solution! This is deliberatedly done in the computeSolution()
     * method, when we do the linear combination of all such matrices
     * to compute the final approximation \f$u_{H,h}\f$ !!!
     */
    Eigen::Block<MSL_MATRIX_FP_TYPE> getEtal() const {
        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        return Eigen::Block<MSL_MATRIX_FP_TYPE>(
                    mDecoratedPtr->getSolution().derived(),
                    0,
                    mDecoratedPtr->getNumberOfRHS(),
                    spaceU->getNDof(),
                    getLambdaTrialDim());
    }
    Eigen::Block<MSL_MATRIX_FP_TYPE> getEtaf() const {
        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        return Eigen::Block<MSL_MATRIX_FP_TYPE>(
                    mDecoratedPtr->getSolution().derived(),
                    0,
                    0,
                    spaceU->getNDof(),
                    mDecoratedPtr->getNumberOfRHS());
    }
    template<typename T = DecoratedProblem,
             typename = typename DecoratedProblem::template EnableIfLagrangeMultipliers<T>>
    Eigen::Block<MSL_MATRIX_FP_TYPE> getEtak() const {
        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        return Eigen::Block<MSL_MATRIX_FP_TYPE>(
                    mDecoratedPtr->getSolution().derived(),
                    0,
                    mDecoratedPtr->getNumberOfRHS()+getLambdaTrialDim(),
                    spaceU->getNDof(),
                    mDecoratedPtr->getDimKernel());
    }

    /*!
     * The same holds for etak when NonUniquenessTreatment::LAGRANGEMULTIPLIERS applies
     * In other cases, we need to solve additional problems to find etak in the basis of
     * the local approximation space
     */
    template<typename T = DecoratedProblem,
             typename = typename DecoratedProblem::template EnableIfLagrangeMultipliers<T>>
    MSL_MATRIX_FP_TYPE genKernelPartOfU(
            const MSL_MATRIX_FP_TYPE& coeff,
            Eigen::Block<MSL_MATRIX_FP_TYPE> etalPlusEtaf) const {
        return getEtak() * coeff;
    }
    template<typename T = DecoratedProblem,
             typename = typename DecoratedProblem::template EnableIfFixDofs<T>>
    MSL_MATRIX_FP_TYPE genKernelPartOfU(
            const MSL_MATRIX_FP_TYPE& coeff,
            const Eigen::Block<MSL_MATRIX_FP_TYPE>& etalPlusEtaf) const {

        Eigen::SparseMatrix<MSL_FP_TYPE> phiTimesKernel, kernelTimesKernel;
        MSL_MATRIX_FP_TYPE etakTimesPhi;
        Eigen::SimplicialLLT<
                Eigen::SparseMatrix<MSL_FP_TYPE>,
                Eigen::Lower,
                Eigen::COLAMDOrdering<int>
                > symSeqSolverType;
        LocalLinearSystem projectionSolver("",
                                           true //symmetric
                                           );

        // 0) Configuring spaces for projection

        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        auto& fixedDofs = mDecoratedPtr->getFixedDofs();
        auto& fixedDofsLabels = mDecoratedPtr->getFixedDofsLabels();
        auto& originalDofsLabels = mDecoratedPtr->getOriginalDofsLabels();
        for (int i = 0; i < fixedDofs.size(); ++i)
            spaceU->setDofType(fixedDofs[i], originalDofsLabels[i]);

        if (!mSpaceU0) {
            LOG(FATAL) << "Kernel space not configured properly. Abort!";
        }

        // 1) Mounting the RHS for the first projetion

        GalerkinLagrangeBlock kernelPhi1(mSpaceU0.get(),
                                         block::INNER_NODE,
                                         mDecoratedPtr->getPkList()[0]+mDecoratedPtr->getKernelPolynomialOrderForIntegration(),
                                         approx_space::U);
        GalerkinLagrangeBlock phi1(spaceU.get(),
                                  block::INNER_NODE,
                                  mDecoratedPtr->getPkList()[0]+mDecoratedPtr->getKernelPolynomialOrderForIntegration(),
                                  approx_space::U);
        ((LocalContribSet)(phi1*kernelPhi1)).assembleSparseMatrix(phiTimesKernel);

        // 2) Mounting the LHS for the first projection

        GalerkinLagrangeBlock kernelPhi0(mSpaceU0.get(),
                                         block::INNER_NODE,
                                         2*mDecoratedPtr->getPkList()[0],
                                         approx_space::U);
        ((LocalContribSet)(kernelPhi0*kernelPhi0)).assembleSparseMatrix(kernelTimesKernel);

        symSeqSolverType.analyzePattern(kernelTimesKernel);
        symSeqSolverType.factorize(kernelTimesKernel);
        if (symSeqSolverType.info() != Eigen::Success) {
            LOG(FATAL)
                << "Solver could not factorize."
                << ". ABORT!";
            exit(1);
        }

        // 3) Solve first linear system and mount the RHS for the second projetion

        etakTimesPhi = phiTimesKernel.transpose() * (coeff - symSeqSolverType.solve(phiTimesKernel*etalPlusEtaf));

        // 4) Mounting the LHS for the second projection

        GalerkinLagrangeBlock phi0(spaceU.get(),
                                  block::INNER_NODE,
                                  2*mDecoratedPtr->getPkList()[0],
                                  approx_space::U);
        projectionSolver.mountA(phi0*phi0);

        // 5) Solving second projection

        projectionSolver.solveAXDenseB(etakTimesPhi);

        // 6) Reconfiguring spaces to the original state

        for (int i = 0; i < fixedDofs.size(); ++i)
            spaceU->setDofType(fixedDofs[i], fixedDofsLabels[i]);

        // 7) Return the solution

        return projectionSolver.getX();
    }
    
private:

    void createLambdaTimesKernel();
    void createSourceTimesKernel();
    
    int getNoInsertPointsByBounSegment() {
        if (mDecoratedPtr->getLkList()[0]==mDecoratedPtr->getPkList()[0]) {
            switch (mDecoratedPtr->getPkList()[0]) {
                case 1: return 3; break;
                case 2: return 2; break;
                default: return 1; break;
            }
        } else if (mDecoratedPtr->getLkList()[0]==mDecoratedPtr->getPkList()[0]-1) {
            return 1;
        } else // mDecoratedPtr->getLk()==mDecoratedPtr->getPkList()[0]-2
            return 0; // no insert Points needed
    }

};

//-------------------------------------------------------------------------
template<typename DecoratedProblem>
void CGMHMDecorator<DecoratedProblem>::buildMesh(mesh_reader::MeshComponents aMeshComps) {
    LOG(DEBUG) << "\nBuilding mesh... ";
    
    if (mDecoratedPtr->isToGenNewMesh()) {
        if (mDecoratedPtr->getNameMesh()=="triangle") {
            
            //TODO: Improve this `if`
            if (mDecoratedPtr->getDomain().getNoNodes()==0) //triangle mesh is generated for a global mesh
                mDecoratedPtr->getMesh().genTriangulation(mDecoratedPtr->getIndexMesh());
            else
                mDecoratedPtr->getMesh().genTriangulation(mDecoratedPtr->getIndexMesh(),
                                                 mDecoratedPtr->getDomain());
            
            //TODO must check this...
            mDecoratedPtr->getDomain().transformRef2RealSimplex(mDecoratedPtr->getMesh().getRefPoints());
        }
        //FJA add the quad case => ! generate a triangular mesh over a quad
        else if (mDecoratedPtr->getNameMesh()=="quadrangle") {
            
            //TODO: Improve this `if`
            if (mDecoratedPtr->getDomain().getNoNodes()==0) //triangle mesh is generated for a global mesh
                mDecoratedPtr->getMesh().genTriangulationOverQuad(mDecoratedPtr->getIndexMesh());
            else
                mDecoratedPtr->getMesh().genTriangulationOverQuad(mDecoratedPtr->getIndexMesh(),
                                                         mDecoratedPtr->getDomain());
            
            //TODO must check this...
            mDecoratedPtr->getDomain().transformRef2Real(mDecoratedPtr->getMesh().getPoints()); //FJA INPROGRESS
            //            LOG(DEBUG) << "Area Quad " << mSuperElem.getMeas();
        }
        //FJA add the general case => ! generate a triangular mesh over a closed polygon
        else if (mDecoratedPtr->getNameMesh()=="polygon") {
            
            //TODO: Improve this `if`
            if (mDecoratedPtr->getDomain().getNoNodes()==0) //triangle mesh is generated for a global mesh
                mDecoratedPtr->getMesh().genTriangulationWithTriangleLib(mDecoratedPtr->getIndexMesh());
            else {
                // Compute area of K, and divide by N^2 (=index_mesh=subelems)
                // N=1,2,4,8,16,32... that will refine the element by 4 at next level
                // except for N=1, producing 2 triangles instead of 1 for quads for example,
                // coeff is a trick to avoid Triangle subdividing if exact area is reached
                double areaSuperElem=mDecoratedPtr->getDomain().getMeas(), coeff=1.01;
                double areaTriangle = areaSuperElem/2./(mDecoratedPtr->getIndexMesh()*mDecoratedPtr->getIndexMesh())*coeff;
#ifndef NDEBUG
                std::cerr << "areaSuperElem " << areaSuperElem
                << " areaTriangle " <<  areaTriangle
                << " estimed #tri " <<  areaSuperElem/areaTriangle
                << " expected #tri " <<  2*mDecoratedPtr->getIndexMesh()*mDecoratedPtr->getIndexMesh()
                << " estimed n " <<  sqrt(areaSuperElem/areaTriangle/2.)
                << " n " << mDecoratedPtr->getIndexMesh()
                << std::endl;
#endif
                
                // automatic local mesh adaptation according to H, l and k
                if (false) {
                    /* H is the edge length in the global mesh
                     * H should have been called h_hat, as we compute over the subivided segment...
                     * H_max for each edge of the SuperElem (ie. SuperFaces).
                     * h_max correspond to the local mesh edge length */
                    double h_max, H_max= std::numeric_limits<double>::min();
                    for (uint i=0; i<mDecoratedPtr->getSuperFaces().size() ; ++i) {
                        H_max=std::max(H_max, mDecoratedPtr->getSuperFaces()[i].getMeas() /
                                              mesh::minDepthOfToken(mDecoratedPtr->getSuperFacePatterns()[i]) );
#ifndef NDEBUG
                        std::cerr << i << " length= " << mDecoratedPtr->getSuperFaces()[i].getMeas()
                        << " minDepth" << mesh::minDepthOfToken(mDecoratedPtr->getSuperFacePatterns()[i]) << " " << H_max << std::endl;
#endif
                    }
                    // l=iLk, k=iPk, for Hdiv norm, O(H^l + h^k-1),
                    // for H1 norm, O(H^l+1 + h^k), L2 norm, O(H^l+2 + h^k+1)
                    double C=0.5;
                    if (mDecoratedPtr->getPkList()[0]>1) { //Compute area_triangle for Hdiv convergence
                        // h_max= std::pow(C,1./(iPk-1.))*std::pow(H_max,(iLk)/(iPk-1.));
                        h_max= std::pow(C*std::pow(H_max,(mDecoratedPtr->getLkList()[0])), 1./(mDecoratedPtr->getPkList()[0]-1.));
                    } else { //Compute area_triangle for H1 convergence only
                        //  h_max= std::pow(C,1./iPk)*std::pow(H_max,(iLk+1.)/(iPk));
                        h_max= std::pow(C*std::pow(H_max,(mDecoratedPtr->getLkList()[0]+1.)), 1./(mDecoratedPtr->getPkList()[0]));
                    }
                    
                    // a=h^2*sqrt(3)/4.0 for equilateral triangle  // 0.433012702
                    areaTriangle = std::min((h_max*h_max*0.433012702), areaTriangle);
#ifndef NDEBUG
                    std::cerr << "h_max " << h_max << " area_max " << h_max*h_max/4. << std::endl;
                    std::cerr << "areaSuperElem " << areaSuperElem
                    << " area_triangle " <<  areaTriangle
                    << " n " << mDecoratedPtr->getIndexMesh()
                    << std::endl;
#endif
                }
                
                char *meshFile;
                meshFile = new char[mDecoratedPtr->getPathTree().size()+15];
                std::strcpy(meshFile, (mDecoratedPtr->getPathTree() +
                                       std::string("/localMesh")).c_str());
                
                mDecoratedPtr->getMesh().genTriangulationWithTriangleLib(
                                        areaTriangle,
                                        mDecoratedPtr->getNoInsertPointsByBounSegment(),
                                        mDecoratedPtr->getDomain(),
                                        mDecoratedPtr->getSuperFaces(),
                                        mDecoratedPtr->getSuperFacePatterns(),
                                        meshFile);
            }
            //FJA: No need to call transformRef2Real for generic polygon (?)
        }
        else if (mDecoratedPtr->getNameMesh()=="tetrahedron") {
            
            LOG(DEBUG) << "Generating mesh for a "
                       << mDecoratedPtr->getNameMesh()
                       <<". Index "
                       << mDecoratedPtr->getIndexMesh();
            
            // Generating mesh files
            
            char *meshFile;
            meshFile = new char[mDecoratedPtr->getPathTree().size()+15];
            std::strcpy(meshFile, (mDecoratedPtr->getPathTree() +
                                   std::string("/localMesh")).c_str());
            
            //mMesh.genTetrahedralization(index_mesh, meshFile);
            
            mDecoratedPtr->getMesh().genTetrahedralization(mDecoratedPtr->getIndexMesh(),
                                        mDecoratedPtr->getDomain(),
                                        meshFile);
            //+Ref2Real            mMesh.genTetrahedralizationOverPolyhedron(index_mesh, mSuperElem, meshFile);
            
            delete [] meshFile;
            
            // Updating local config_mhm
            
            std::ifstream readFile((mDecoratedPtr->getPathTree() + std::string("/config_mhm")).c_str(), std::ifstream::in);
            std::ofstream outFile((mDecoratedPtr->getPathTree() + std::string("/config_mhm_aux")).c_str(), std::ofstream::out);
            
            std::string line;
            std::getline(readFile,line);
            outFile << mDecoratedPtr->getPathTree() + std::string(" localMesh 0\n");
            
            while(std::getline(readFile,line))
                outFile << line << "\n";
            
            std::remove( (mDecoratedPtr->getPathTree() + std::string("/config_mhm")).c_str() );
            std::rename( (mDecoratedPtr->getPathTree() + std::string("/config_mhm_aux")).c_str() ,
                        (mDecoratedPtr->getPathTree() + std::string("/config_mhm")).c_str() );
            
            //TODO must check this...
            mDecoratedPtr->getDomain().transformRef2RealSimplex(mDecoratedPtr->getMesh().getRefPoints());
            
        }
        else if (mDecoratedPtr->getNameMesh()=="hexahedron") {
            
            LOG(DEBUG) << "Generating mesh for a "
                       << mDecoratedPtr->getNameMesh()
                       <<". Index "
                        << mDecoratedPtr->getIndexMesh();
            
            // Generating mesh files
            
            char *meshFile;
            meshFile = new char[mDecoratedPtr->getPathTree().size()+15];
            std::strcpy(meshFile, (mDecoratedPtr->getPathTree() +
                                   std::string("/localMesh")).c_str());
            
            mDecoratedPtr->getMesh().genTetrahedralizationOverPolyhedron(mDecoratedPtr->getIndexMesh(),
                                                      mDecoratedPtr->getDomain(),
                                                      meshFile);
            
            delete [] meshFile;
            
            // Updating local config_mhm
            
            std::ifstream readFile((mDecoratedPtr->getPathTree() + std::string("/config_mhm")).c_str(), std::ifstream::in);
            std::ofstream outFile((mDecoratedPtr->getPathTree() + std::string("/config_mhm_aux")).c_str(), std::ofstream::out);
            
            std::string line;
            std::getline(readFile,line);
            outFile << mDecoratedPtr->getPathTree() + std::string(" localMesh 0\n");
            
            while(std::getline(readFile,line))
                outFile << line << "\n";
            
            std::remove( (mDecoratedPtr->getPathTree() + std::string("/config_mhm")).c_str() );
            std::rename( (mDecoratedPtr->getPathTree() + std::string("/config_mhm_aux")).c_str() ,
                        (mDecoratedPtr->getPathTree() + std::string("/config_mhm")).c_str() );
            
            //FJA: No need to call transformRef2Real for generic polyhedron (?)
            //mSuperElem.transformRef2Real(mMesh.getRefPoints());
            
        }
        else if (mDecoratedPtr->getNameMesh()=="localMesh") {
            mDecoratedPtr->getMesh().load(mDecoratedPtr->getPathTree().c_str(),"localMesh",0,0,aMeshComps);
            
            //TODO must check this...
            mDecoratedPtr->getDomain().transformRef2RealSimplex(mDecoratedPtr->getMesh().getRefPoints());
        }
    }
    else {
        mDecoratedPtr->getMesh().load(mDecoratedPtr->getPathMesh().c_str(),
                             mDecoratedPtr->getNameMesh().c_str(),
                             mDecoratedPtr->getIndexMesh(),
                             1,
                             aMeshComps);
    }
    
    if (aMeshComps & mesh_reader::EDGE) {
        mDecoratedPtr->getMesh().invert(); //FJA only used in identifyEdges2D and 3D
        
        switch (mDecoratedPtr->getMesh().getMeshDim()) {
            case 2:
                mDecoratedPtr->getMesh().identifyEdges2D();
                break;
                
            case 3:
                if (aMeshComps & mesh_reader::FACE)
                    mDecoratedPtr->getMesh().identifyFaces3D();
                
                mDecoratedPtr->getMesh().identifyEdges3D();
                break;
                
            default:
                break;
        }
        
        mDecoratedPtr->getMesh().deleteInverse(); // delete mInvElems
    }
    
    LOG(DEBUG) << "done!\n";
}

//-------------------------------------------------------------------------
template<typename DecoratedProblem>
void CGMHMDecorator<DecoratedProblem>::configureLambdaSpace() {

    mLambdaSpace = std::make_shared<MHMSpace>();

    if(SpaceOrderTrait<DecoratedProblem>::space_order !=
       QuantityType::SCALAR_QUANTITY)
        mLambdaSpace->setVarType(
                    TensorType(SpaceOrderTrait<DecoratedProblem>::space_order,
                               mDecoratedPtr->getMesh().getMeshDim())
                    );

    for(auto it = mDecoratedPtr->getDirichletBC().begin();
        it != mDecoratedPtr->getDirichletBC().end();
        ++it)
        this->mLambdaSpace->FiniteElementSpace::setEssentialBC(it->second,
                                                                 it->first);
    for(auto it = mDecoratedPtr->getNeumannBC().begin();
        it != mDecoratedPtr->getNeumannBC().end();
        ++it)
        this->mLambdaSpace->FiniteElementSpace::setEssentialBC(it->second,
                                                                     it->first);
}

//------------------------------------------------------------------------------
template<typename DecoratedProblem>
void CGMHMDecorator<DecoratedProblem>::createLambdaRHS() {
    
    LambdaData PsiFunction;
    
    auto &spaceU = mDecoratedPtr->getSpaceList()[0];
    std::shared_ptr<LocalContribSet> decoratedRHS = mDecoratedPtr->DecoratedProblem::getRHS();
    int sizeRHS = decoratedRHS->getTotalTrialSize();

    if (!mLambdaSpace) configureLambdaSpace();
    
    // Creating 'phi' block for P^iPk functions on the boundary
    
    GalerkinLagrangeBlock BoundaryPhi(
                  spaceU.get(),
                  block::BOUNDARY_NODE,
                  mDecoratedPtr->getPkList()[0]+mDecoratedPtr->getLkList()[0],
                  approx_space::U);
        
    PsiFunction.setSpace(*mLambdaSpace);
    PsiFunction.create(mDecoratedPtr->getSuperFacePatterns(),
                       mDecoratedPtr->getSuperFaces(),
                       mDecoratedPtr->getLkList()[0],
                       mDecoratedPtr->getSigns());

    *decoratedRHS += PsiFunction*BoundaryPhi;

    this->mLambdaTrialDim = decoratedRHS->getTotalTrialSize() - sizeRHS;
}

//------------------------------------------------------------------------------
template<typename DecoratedProblem>
void CGMHMDecorator<DecoratedProblem>::createKernelRHS() {

    if (NonUniquenessTrait<DecoratedProblem>::treatment == NonUniquenessTreatment::FIXDOFS) {

        /*
         * When the strategy FIXDOFS is using, we do not have additional RHS but,
         * instead, we must assure the compatibility condition between source and
         * boundary conditions hold
         */

        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        if (!mSpaceU0) {
            LOG(DEBUG) << "creating kernel space";
            mLocalSpaceU0 = mDecoratedPtr->genLocalKernelSpace();
            mSpaceU0 = std::make_shared<StdFiniteElementSpace>(mDecoratedPtr->getMesh(), *(mLocalSpaceU0.get()));
            LOG(DEBUG) << "done!";
        }

        Eigen::SparseMatrix<MSL_FP_TYPE> phiTimesKernel, kernelTimesKernel, decoratedRHS;
        Eigen::SimplicialLLT<
                Eigen::SparseMatrix<MSL_FP_TYPE>,
                Eigen::Lower,
                Eigen::COLAMDOrdering<int>
                > symSeqSolverType;

        // Creating RHS for the kernel system

        createSourceTimesKernel();
        createLambdaTimesKernel();

        // Mounting linear system

        // Creating 'phi' blocks for kernel functions
        GalerkinLagrangeBlock kernelPhi(mSpaceU0.get(),
                                         block::INNER_NODE,
                                         2*mDecoratedPtr->getKernelPolynomialOrderForIntegration(),
                                         approx_space::U);

//        //TODO: Verify the time spent here!!
//        LocalLinearSystem AXB = LocalLinearSystem("",true); //Symmetric LHS
//        AXB.mountA(multiplier*multiplier);

        // Eigen version:
        ((LocalContribSet)(kernelPhi*kernelPhi)).assembleSparseMatrix(kernelTimesKernel);
        symSeqSolverType.analyzePattern(kernelTimesKernel);
        symSeqSolverType.factorize(kernelTimesKernel);
        if (symSeqSolverType.info() != Eigen::Success) {
            LOG(FATAL)
                << "Solver could not factorize."
                << ". ABORT!";
            exit(1);
        }

        // Mounting the matrix phi * kernel

        GalerkinLagrangeBlock kernelPhi2(mSpaceU0.get(),
                                         block::INNER_NODE,
                                         mDecoratedPtr->getPkList()[0]+mDecoratedPtr->getKernelPolynomialOrderForIntegration(),
                                         approx_space::U);
        GalerkinLagrangeBlock phi(spaceU.get(),
                                  block::INNER_NODE,
                                  mDecoratedPtr->getPkList()[0]+mDecoratedPtr->getKernelPolynomialOrderForIntegration(),
                                  approx_space::U);

        ((LocalContribSet)(kernelPhi2*phi)).assembleSparseMatrix(phiTimesKernel);

        // Mounting the final RHS matrix

        const Eigen::SparseMatrix<MSL_FP_TYPE>& L = sourceTimesKernel;
        const Eigen::SparseMatrix<MSL_FP_TYPE>& R = lambdaTimesKernel;
        Eigen::SparseMatrix<MSL_FP_TYPE> auxM(L.rows(), L.cols()+R.cols());
        auxM.reserve(L.nonZeros() + R.nonZeros());
        for(Eigen::Index c=0; c<L.outerSize(); ++c) {
            auxM.startVec(c); // Important: Must be called once for each col before inserting!
            for(Eigen::SparseMatrix<MSL_FP_TYPE>::InnerIterator itL(L, c); itL; ++itL)
                 auxM.insertBack(itL.row(), c) = itL.value();
        }
        for(Eigen::Index c=L.outerSize(); c<L.outerSize()+R.outerSize(); ++c) {
            auxM.startVec(c); // Important: Must be called once for each col before inserting!
            for(Eigen::SparseMatrix<MSL_FP_TYPE>::InnerIterator itR(R, c-L.outerSize()); itR; ++itR)
                 auxM.insertBack(itR.row(), c) = itR.value();
        }
        auxM.finalize();

        mDecoratedPtr->DecoratedProblem::getRHS()->assembleSparseMatrix(decoratedRHS);
//        AXB.solveAXB(sourceTimesKernel+lambdaTimesKernel);
//        mRHS = std::make_shared<MSL_MATRIX_FP_TYPE>(
//                    decoratedRHS - phiTimesKernel * AXB.getX());
        mRHS = decoratedRHS - phiTimesKernel * symSeqSolverType.solve(auxM);

    } else {

        CHECK(mDecoratedPtr->getSpaceList().size()>1) << "Cannot create kernel RHS without a corresponding approximation space. ABORT!";
        auto mSpaceU0 = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(mDecoratedPtr->getSpaceList().end()-1));

        // Creating 'phi' blocks for kernel functions
        GalerkinLagrangeBlock kernelPhi(mSpaceU0.get(),
                                         block::INNER_NODE,
                                         2*mDecoratedPtr->getKernelPolynomialOrderForIntegration(),
                                         approx_space::U);

        *mDecoratedPtr->DecoratedProblem::getRHS() += kernelPhi*kernelPhi;
    }
}



//------------------------------------------------------------------------------
template<typename DecoratedProblem>
void CGMHMDecorator<DecoratedProblem>::createLambdaTimesKernel() {

    LambdaData PsiFunction;
    LocalContribSet aLocalContribSet;

    if (!mLambdaSpace) configureLambdaSpace();
    if (!mSpaceU0) {
        LOG(DEBUG) << "creating kernel space";
        mLocalSpaceU0 = mDecoratedPtr->genLocalKernelSpace();
        mSpaceU0 = std::make_shared<StdFiniteElementSpace>(mDecoratedPtr->getMesh(), *(mLocalSpaceU0.get()));
        LOG(DEBUG) << "done!";
    }

    // Creating 'phi' block for P^iPk functions on the boundary

    GalerkinLagrangeBlock BoundaryPhi(
                  mSpaceU0.get(),
                  block::BOUNDARY_NODE,
                  mDecoratedPtr->getKernelPolynomialOrderForIntegration()+mDecoratedPtr->getLkList()[0],
                  approx_space::U);

    PsiFunction.setSpace(*mLambdaSpace);
    PsiFunction.create(mDecoratedPtr->getSuperFacePatterns(),
                       mDecoratedPtr->getSuperFaces(),
                       mDecoratedPtr->getLkList()[0],
                       mDecoratedPtr->getSigns());

    aLocalContribSet = PsiFunction*BoundaryPhi;

    aLocalContribSet.assembleSparseMatrix(lambdaTimesKernel);
}

//------------------------------------------------------------------------------
template<typename DecoratedProblem>
void CGMHMDecorator<DecoratedProblem>::createSourceTimesKernel() {

    if (!mSpaceU0) {
        LOG(DEBUG) << "creating kernel space";
        mLocalSpaceU0 = mDecoratedPtr->genLocalKernelSpace();
        mSpaceU0 = std::make_shared<StdFiniteElementSpace>(mDecoratedPtr->getMesh(), *(mLocalSpaceU0.get()));
        LOG(DEBUG) << "done!";
    }

    // Creating 'phi' block for P^iPk functions on the boundary

    GalerkinLagrangeBlock phi(
                  mSpaceU0.get(),
                  block::INNER_NODE,
                  mDecoratedPtr->getKernelPolynomialOrderForIntegration() + mDecoratedPtr->getIntegrationOrderForSource(),
                  approx_space::U);

    ((LocalContribSet)(DataBlock(*mDecoratedPtr->getSourceFunction())*phi)).assembleSparseMatrix(sourceTimesKernel);
}

//------------------------------------------------------------------------------
template<typename DecoratedProblem>
std::vector<double> CGMHMDecorator<DecoratedProblem>::genContribSourceTimesKernel() {

    std::vector<double> contrib;

    if (NonUniquenessTrait<DecoratedProblem>::treatment == NonUniquenessTreatment::FIXDOFS) {
        contrib.resize(sourceTimesKernel.size(),0.0);
        const Eigen::SparseMatrix<MSL_FP_TYPE>& A = sourceTimesKernel;
        for(Eigen::Index c=0; c<A.outerSize(); ++c)
            for(Eigen::SparseMatrix<MSL_FP_TYPE>::InnerIterator itL(A, c); itL; ++itL)
                 contrib[c*A.rows()+itL.row()] = -itL.value();
    } else {

        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        auto &X = mDecoratedPtr->getSolution();
        auto &B = mDecoratedPtr->getLoadVector();

        int l_dim = getLambdaTrialDim();
        int k_dim = mDecoratedPtr->getDimKernel();
        int f_dim = mDecoratedPtr->getNumberOfRHS();
        int num_rows = spaceU->getNDof();

        MSL_MATRIX_FP_TYPE matrixContrib = X.block(0,f_dim+l_dim,num_rows,k_dim).transpose()
                * B.block(0,0,num_rows,f_dim);
        contrib.resize(matrixContrib.size());
        for (int j=0; j<matrixContrib.cols(); ++j)
            for (int i=0; i<matrixContrib.rows(); ++i)
                contrib[j*matrixContrib.rows()+i] = -matrixContrib(i,j);
    }

    return contrib;
}
//------------------------------------------------------------------------------
template<typename DecoratedProblem>
std::vector<double> CGMHMDecorator<DecoratedProblem>::genContribLambdaTimesKernel() {

    std::vector<double> contrib;

    if (NonUniquenessTrait<DecoratedProblem>::treatment == NonUniquenessTreatment::FIXDOFS) {
        contrib.resize(lambdaTimesKernel.size(),0.0);
        const Eigen::SparseMatrix<MSL_FP_TYPE>& A = lambdaTimesKernel;
        for(Eigen::Index c=0; c<A.outerSize(); ++c)
            for(Eigen::SparseMatrix<MSL_FP_TYPE>::InnerIterator itL(A, c); itL; ++itL)
                 contrib[c*A.rows()+itL.row()] = itL.value();
    } else {

        auto &spaceU = mDecoratedPtr->getSpaceList()[0];
        auto &X = mDecoratedPtr->getSolution();
        auto &B = mDecoratedPtr->getLoadVector();

        int l_dim = getLambdaTrialDim();
        int k_dim = mDecoratedPtr->getDimKernel();
        int f_dim = mDecoratedPtr->getNumberOfRHS();
        int num_rows = spaceU->getNDof();

        MSL_MATRIX_FP_TYPE matrixContrib = X.block(0,f_dim+l_dim,num_rows,k_dim).transpose()
                * B.block(0,f_dim,num_rows,l_dim);
        contrib.resize(matrixContrib.size());
        for (int j=0; j<matrixContrib.cols(); ++j)
            for (int i=0; i<matrixContrib.rows(); ++i)
                contrib[j*matrixContrib.rows()+i] = matrixContrib(i,j);
    }

    return contrib;
}
//------------------------------------------------------------------------------
template<typename DecoratedProblem>
std::vector<double> CGMHMDecorator<DecoratedProblem>::genContribLambdaTimesEtaf() {

    std::vector<double> contrib;

    auto &spaceU = mDecoratedPtr->getSpaceList()[0];
    auto &X = mDecoratedPtr->getSolution();

    int l_dim = getLambdaTrialDim();
    int f_dim = mDecoratedPtr->getNumberOfRHS();
    int num_rows = spaceU->getNDof();

    MSL_MATRIX_FP_TYPE matrixContrib;
    if (mDecoratedPtr->getDimKernel() > 0 &&
            NonUniquenessTrait<DecoratedProblem>::treatment == NonUniquenessTreatment::FIXDOFS)
        matrixContrib = X.block(0,0,num_rows,f_dim).transpose() * mRHS.block(0,f_dim,num_rows,l_dim);
    else
        matrixContrib = X.block(0,0,num_rows,f_dim).transpose() * mDecoratedPtr->getLoadVector().block(0,f_dim,num_rows,l_dim);
    
    contrib.resize(matrixContrib.size());
    for (int j=0; j<matrixContrib.cols(); ++j)
        for (int i=0; i<matrixContrib.rows(); ++i)
            contrib[j*matrixContrib.rows()+i] = -matrixContrib(i,j);

    return contrib;
}
//------------------------------------------------------------------------------
template<typename DecoratedProblem>
std::vector<double> CGMHMDecorator<DecoratedProblem>::genContribLambdaTimesEtaL() {

    std::vector<double> contrib;

    auto &spaceU = mDecoratedPtr->getSpaceList()[0];
    auto &X = mDecoratedPtr->getSolution();

    int l_dim = getLambdaTrialDim();
    int f_dim = mDecoratedPtr->getNumberOfRHS();
    int num_rows = spaceU->getNDof();

    MSL_MATRIX_FP_TYPE matrixContrib;
    if (mDecoratedPtr->getDimKernel() > 0 &&
            NonUniquenessTrait<DecoratedProblem>::treatment == NonUniquenessTreatment::FIXDOFS)
        matrixContrib = X.block(0,f_dim,num_rows,l_dim).transpose() * mRHS.block(0,f_dim,num_rows,l_dim);
    else
        matrixContrib = X.block(0,f_dim,num_rows,l_dim).transpose() * mDecoratedPtr->getLoadVector().block(0,f_dim,num_rows,l_dim);
    
    contrib.resize(matrixContrib.size());
    for (int j=0; j<matrixContrib.cols(); ++j)
        for (int i=0; i<matrixContrib.rows(); ++i)
            contrib[j*matrixContrib.rows()+i] = matrixContrib(i,j);

    return contrib;
}

#endif /* __FACADE_PROBLEMMHMCGDECORATOR_H__ */

