#!/bin/bash
#
# This script collapses all tree/*/{micromesh.geo,solution.nsca} files to tree/MHMSolution.{geo,nsca}
# This is to help on rendering with Paraview...
#
# $Id: reduce_micromeshes_to_single_mesh.sh 1489 2018-11-13 20:18:20Z weslleyp $
#

###
### Configuration
###

verbose=1
printPeriodInMultiplesOf=100

genGeo=1
genCase=1
genNSCA=1

# Select between binary or non binary formats
binaryGeo=$2

# Variables
#solutions=(solution.nsca dtSolution.nsca)
#solutions=(solution.nsca vonMises.esca)
solutions=(solution.nsca)

# Testing option in nsca
accumulateInString=0

# Step for transient solution
step=1
step0=0
#N=10
#nZ=4

# Auxiliar path
auxFolder=".reduce_micromeshes_to_single_mesh"
if [ -d "$1/$auxFolder" ]; then
  echo "WARNING: Folder was created before!"
else
  mkdir "$1/$auxFolder"
fi

###
### Auxiliar things
###

starFiller='******************************************************************************'

###
### Move / remove old files
###

localDir="$(pwd)"
cd "$1/$auxFolder"

mv "../splitlog" . 2>/dev/null
mv ../*.txt . 2>/dev/null
mv "../MHMSolution.case" . 2>/dev/null
mv "../MHMSolution.geo" . 2>/dev/null
mv "../hpc4ePlot" . 2>/dev/null
for solution in ${solutions[@]}; do mv ../$solution* .; done

## Move all files but numeric ones

#if [ -d "./$auxFolder" ]; then
#  echo "WARNING: Folder was created before!"
#else
#  mkdir "./$auxFolder"
#fi

#mv * "./$auxFolder"
#cd "./$auxFolder"
#mv $(ls [0-9]* -d) ..

# Correct print debug
printPeriodInMultiplesOf=$((printPeriodInMultiplesOf-1))

###
### .case file
###

if [ $genCase -eq 1 ]; then
    if [ $verbose -eq 1 ]; then printf "\nGenerating MHMSolution.case"; fi

    printf "$(cat ../0/micromesh.case)" > MHMSolution.case
    sed 's/micromesh/MHMSolution/' MHMSolution.case > tmp && mv tmp MHMSolution.case
#    sed 's/solution/MHMSolution/' MHMSolution.case > tmp && mv tmp MHMSolution.case
fi

###
### .geo file
###

if [ $genGeo -eq 1 ]; then
    if [ $verbose -eq 1 ]; then printf "\nGenerating MHMSolution.geo"; fi
    
    echo -n "" > MHMSolution.geo # clean file
    exec 3<> MHMSolution.geo # open file description 3
    
    if [ $binaryGeo -eq 1 ]; then
        buffer="C Binary"
        echo -n $buffer >&3
        echo -n -e '\x00' >&3
        echo "${starFiller:${#buffer}}" >&3
    fi
    
    descriptionArray=("Geometry File" "Problem Dataset" "node id off" "element id off")
    for i in {0..3}; do
        buffer=${descriptionArray[i]}
        echo -n $buffer >&3
        if [ $binaryGeo -eq 1 ]; then
            echo -n -e '\x00' >&3
            echo "${starFiller:${#buffer}}" >&3
        else
            echo "" >&3
        fi
    done

#    if [ $binaryGeo -eq 1 ]; then
#    
#        if [ $binaryGeo -eq 1 ]; then
#            buffer="C Binary"
#            echo -n $buffer >&3
#            echo -n -e '\x00' >&3
#            for i in $(seq ${#buffer} 1 77); do echo -n -e '*' >&3; done
#            echo "" >&3
#        fi
#        
#        buffer="Geometry File"
#        echo -n $buffer >&3
#        if [ $binaryGeo -eq 1 ]; then
#            echo -n -e '\x00' >&3
#            for i in $(seq ${#buffer} 1 77); do echo -n -e '*' >&3; done
#        fi
#        echo "" >&3
#        
#        buffer="Problem Dataset"
#        echo -n $buffer >&3
#        if [ $binaryGeo -eq 1 ]; then
#            echo -n -e '\x00' >&3
#            for i in $(seq ${#buffer} 1 77); do echo -n -e '*' >&3; done
#        fi
#        echo "" >&3
#        
#        buffer="node id off"
#        echo -n $buffer >&3
#        if [ $binaryGeo -eq 1 ]; then
#            echo -n -e '\x00' >&3
#            for i in $(seq ${#buffer} 1 77); do echo -n -e '*' >&3; done
#        fi
#        echo "" >&3
#        
#        buffer="element id off"
#        echo -n $buffer >&3
#        if [ $binaryGeo -eq 1 ]; then
#            echo -n -e '\x00' >&3
#            for i in $(seq ${#buffer} 1 77); do echo -n -e '*' >&3; done
#        fi
#        echo "" >&3
#    else
#        printf "Geometry File\nProblem 3D Dataset\nnode id off\nelement id off" >&3
#    fi
    

    count=0
    for idx_subproblem in $(ls -v ..); do # -v option sorts the folders by number
#    for idx_subproblem in $(seq 0 1 0); do
        
        if [ $binaryGeo -eq 1 ]; then
        
            buffer="part"
            echo -n $buffer >&3
            echo -n -e '\x00' >&3
            echo "${starFiller:${#buffer}}" >&3
            
#            # Big-endian
#            printf "0: %.8x" $((idx_subproblem+1)) | xxd -r -g0 >&3

            # Little-endian (C-like)
            printf "0: %.8x" $((idx_subproblem+1)) | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r -g0 >&3
        
            buffer="Mesh-"$idx_subproblem
            echo -n $buffer >&3
            echo -n -e '\x00' >&3
            echo "${starFiller:${#buffer}}" >&3
            
            tail -c +565 ../$idx_subproblem/micromesh.geo >&3
#            echo -n -e $(tail -c +565 ../$idx_subproblem/micromesh.geo | hexdump -C) >&3
        else
            printf "\npart\n%7d\nMesh-%d\n" $((idx_subproblem+1)) $idx_subproblem >&3
            tail -n +8 ../$idx_subproblem/micromesh.geo >&3
        fi

        if [ $verbose -eq 1 ]; then
            if [ $count -eq $printPeriodInMultiplesOf ]; then printf "."; count=0
            else count=$((count+1)); fi
        fi
    done

    exec 3>&- # close file description 3

fi

###
### .nsca file(s)
###

if [ $genNSCA -eq 1 ]; then

    if [ $(ls ../0/${solutions[0]} | wc -l) -eq 1 ]; then
    
        for solution in ${solutions[@]}; do
            if [ $verbose -eq 1 ]; then printf "\nGenerating $solution"; fi
            
            echo -n "" > $solution # clean file
            exec 3<> $solution # open file description 3

                # Print description
                buffer="Values for EnSight Gold"
                echo -n $buffer >&3
                if [ $binaryGeo -eq 1 ]; then
                    echo -n -e '\x00' >&3
                    echo "${starFiller:${#buffer}}" >&3
                else
                    echo "" >&3
                fi

                # Loop through files
                count=0
                if [ $binaryGeo -eq 1 ]; then
                    for idx_subproblem in $(ls -v ..); do # -v option sorts the folders by number
    
                        buffer="part"
                        echo -n $buffer >&3
                        echo -n -e '\x00' >&3
                        echo "${starFiller:${#buffer}}" >&3
                        
            #            # Big-endian
            #            printf "0: %.8x" $((idx_subproblem+1)) | xxd -r -g0 >&3

                        # Little-endian (C-like)
                        printf "0: %.8x" $((idx_subproblem+1)) | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r -g0 >&3
                        
                        tail -c +165 "../$idx_subproblem/$solution" >&3

                        if [ $verbose -eq 1 ]; then
                            if [ $count -eq $printPeriodInMultiplesOf ]; then
                                printf "."; count=0
                            else count=$((count+1)); fi
                        fi

                    done
                else
                    for idx_subproblem in $(ls -v ..); do # -v option sorts the folders by number

                        printf "part\n%7d\n" $((idx_subproblem+1)) >&3
                        tail -n +4 "../$idx_subproblem/$solution" >&3
                        printf "\n" >&3

                        if [ $verbose -eq 1 ]; then
                            if [ $count -eq $printPeriodInMultiplesOf ]; then
                                printf "."; count=0
                            else count=$((count+1)); fi
                        fi

                    done
                fi

            exec 3>&- # close file description 3
            
        done

    else
    
        for solution in ${solutions[@]}; do

            if [ -d "../0/" ]; then
              cd ../0/
            else
              echo "0 directory does not exist. Abort!"
              exit
            fi

#                # Count the number of time steps
#                N=$(ls $solution* | wc -l)

#                # Subtracts one, since it starts in 0
#                N=$((N-1))

                # Last solution time
                if [ -z $N ]; then
                    N=$(ls $solution* | tail -1)
                    N=${N/$solution/}
                fi

                # Verify the number format
                if [ -z $nZ ]; then
                    nZ=$(grep -o '0' <<< $(ls $solution* | head -1) | wc -l)
                fi

            cd -

            for k in $(seq $step0 $step $N); do

                kStr=""
                if [ $nZ -eq 1 ]; then kStr=$(printf "%01d" $k)
                else if [ $nZ -eq 2 ]; then kStr=$(printf "%02d" $k)
                else if [ $nZ -eq 3 ]; then kStr=$(printf "%03d" $k)
                else if [ $nZ -eq 4 ]; then kStr=$(printf "%04d" $k)
                fi; fi; fi; fi

                solutionk=$solution$kStr

                if [ $verbose -eq 1 ]; then printf "\nGenerating $solutionk"; fi

    #if [ $accumulateInString -eq 1 ]; then

    #            # Create sol string
    #            mhmSolStr="Per_node values for EnSight Gold"

    #            # Loop through files
    #            count=0
    #            for idx_subproblem in $(ls -v $1); do # -v option sorts the folders by number

    #                mhmSolStr+=$(printf "\npart\n%7d\n" $((idx_subproblem+1)))$(tail -n +4 "$1/$idx_subproblem/solution.nsca$kStr")

    #                if [ $verbose -eq 1 ]; then
    #                    if [ $count -eq 999 ]; then printf "."; count=0
    #                    else count=$((count+1)); fi
    #                fi

    #            done

    #            printf "$mhmSolStr" > $mhmSol

    #else

                echo -n "" > $solutionk # clean file
                exec 3<> $solutionk # open file description 3

                    # Print description
                    buffer="Values for EnSight Gold"
                    echo -n $buffer >&3
                    if [ $binaryGeo -eq 1 ]; then
                        echo -n -e '\x00' >&3
                        echo "${starFiller:${#buffer}}" >&3
                    else
                        echo "" >&3
                    fi

                    # Loop through files
                    count=0
                    if [ $binaryGeo -eq 1 ]; then
                        for idx_subproblem in $(ls -v ..); do # -v option sorts the folders by number
        
                            buffer="part"
                            echo -n $buffer >&3
                            echo -n -e '\x00' >&3
                            echo "${starFiller:${#buffer}}" >&3
                            
                #            # Big-endian
                #            printf "0: %.8x" $((idx_subproblem+1)) | xxd -r -g0 >&3

                            # Little-endian (C-like)
                            printf "0: %.8x" $((idx_subproblem+1)) | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r -g0 >&3
                            
                            tail -c +165 "../$idx_subproblem/$solutionk" >&3

                            if [ $verbose -eq 1 ]; then
                                if [ $count -eq $printPeriodInMultiplesOf ]; then
                                    printf "."; count=0
                                else count=$((count+1)); fi
                            fi

                        done
                    else
                        for idx_subproblem in $(ls -v ..); do # -v option sorts the folders by number

                            printf "part\n%7d\n" $((idx_subproblem+1)) >&3
                            tail -n +4 "../$idx_subproblem/$solutionk" >&3
                            printf "\n" >&3

                            if [ $verbose -eq 1 ]; then
                                if [ $count -eq $printPeriodInMultiplesOf ]; then
                                    printf "."; count=0
                                else count=$((count+1)); fi
                            fi

                        done
                    fi

                exec 3>&- # close file description 3

    #fi
            done
            
        done
    fi
fi

###
### Moving files back to the folder
###

mv "splitlog" .. 2>/dev/null
mv *.txt .. 2>/dev/null
mv "MHMSolution.case" .. 2>/dev/null
mv "MHMSolution.geo" .. 2>/dev/null
mv "hpc4ePlot" .. 2>/dev/null
for solution in ${solutions[@]}; do mv $solution* ..; done

cd ..
rm -rf "$auxFolder"

echo
