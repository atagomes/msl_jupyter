/*!*****************************************************************************
 *
 * @file problem_cgdiffusion.cpp
 * MSL_CG -- Example of facade usage
 *
 * @author Antonio Tadeu Gomes
 * @author Diego Paredes
 * @author Weslley Pereira
 *
 * @copyright Copyright 2013-2021 Laboratorio Nacional de Computacao Cientifica (LNCC)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the 
 * Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the 
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @date 27/6/14
 *
 ******************************************************************************/

#include "problem_cgdiffusion.h"

//C standard libraries

//C++ standard libraries
#include <cmath>
#include <memory>

//Other libraries
#include "Easylogging++/include/easylogging++.h"

//Project header files
#include "ApproxSpace/space_stdfiniteelem.h"
#include "Assemble/local_contrib_set.h"
#include "Block/block_galerkinlagrange.h"
#include "Block/weak_form_expression.h"
#include "Data/data.h"
#include "Data/data_function.h"

#ifdef USE_MPI
#include "LinearSystem/linear_system_distmumps.h"
#else
#include "LinearSystem/linear_system.h"
#endif

#include "Utils/lua_script.h"

//------------------------------------------------------------------------------
//----------------------------- PROBLEM ----------------------------------------
//------------------------------------------------------------------------------

const std::string DiffusionCGProblem::name = "diffusion";

////----------------------------------------------------------------------------
////This is where the definition of error computation must be programmed...
////----------------------------------------------------------------------------
DiffusionCGProblem::DiffusionCGProblem(const std::shared_ptr<mesh_reader::MeshReader> &aMeshReader)
: Problem(aMeshReader)
, iPf(3)
, iPkappa(0)
, mSolution(0,0)
{
    mErrorFunctions.insert(std::make_pair("L2",
                                          [](Problem &p, int i_order) -> MSL_FP_TYPE
    {
                                              
        DiffusionCGProblem& dcgp = static_cast<DiffusionCGProblem&>(p);
        
        std::shared_ptr<FunctionData> func = std::dynamic_pointer_cast<FunctionData>(dcgp.u);
        if (!func) {
            LOG(FATAL) << "Function is missing!";
            exit(1);
        }
        
        std::shared_ptr<StdFiniteElementSpace> spaceU(NULL);
        if (dcgp.mSpaceList.empty()) {
            dcgp.configureSpaces();
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
        else {
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
        
        if (i_order<=0) i_order = 2*dcgp.iPu;
        if (i_order < 2*dcgp.mPkList[0]) i_order = 2*dcgp.mPkList[0];
        
        if (dcgp.mSolution.rows()==0)
            dcgp.solve();
        
        //To print a solution we must account for the fact that the
        //partitioning strategy might have changed the DoF numbering
        //so that their indexes do not correspond to the indexes of the
        //geometric entities (vertices, elements etc), and we must recover
        //such correspondence before printing!
        MSL_VECTOR_FP_TYPE sol(dcgp.mSolution.rows());
        if (dcgp.mPartitionInfo == NULL)
            sol = dcgp.mSolution.block(0, 0, dcgp.mSolution.rows(), 1);
        else {
            auto it_beg = dcgp.mPartitionInfo->total_dof_begin();
            auto it_end = dcgp.mPartitionInfo->total_dof_end();
            for (auto it = it_beg; it != it_end; ++it) {
                if (*it<dcgp.mSolution.rows())
                    sol[*it] = dcgp.mSolution(std::distance(it_beg, it), 0);
            }
        }
        
        MSL_FP_TYPE error = spaceU->errorL2_squared(Vector(sol),
                                                    *func,
                                                    i_order,
                                                    dcgp.mPartitionInfo.get());
        return sqrt(error);
                                              
    }));
    
    mErrorFunctions.insert(std::make_pair("semiH1",
                                          [](Problem &p, int i_order) -> MSL_FP_TYPE
    {
        
        DiffusionCGProblem& dcgp = static_cast<DiffusionCGProblem&>(p);
        
        std::shared_ptr<FunctionData> func = std::dynamic_pointer_cast<FunctionData>(dcgp.du);
        if (!func) {
            LOG(FATAL) << "Function is missing!";
            exit(1);
        }
        
        std::shared_ptr<StdFiniteElementSpace> spaceU(NULL);
        if (dcgp.mSpaceList.empty()) {
            dcgp.configureSpaces();
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
        else {
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
        
        if (i_order<=0) i_order = 2*dcgp.iPdu;
        if (i_order < 2*(dcgp.mPkList[0]-1)) i_order = 2*(dcgp.mPkList[0]-1);
        
        if (dcgp.mSolution.rows()==0)
            dcgp.solve();
        
        //To print a solution we must account for the fact that the
        //partitioning strategy might have changed the DoF numbering
        //so that their indexes do not correspond to the indexes of the
        //geometric entities (vertices, elements etc), and we must recover
        //such correspondence before printing!
        MSL_VECTOR_FP_TYPE sol(dcgp.mSolution.rows());
        if (dcgp.mPartitionInfo == NULL)
            sol = dcgp.mSolution.block(0, 0, dcgp.mSolution.rows(), 1);
        else {
            auto it_beg = dcgp.mPartitionInfo->total_dof_begin();
            auto it_end = dcgp.mPartitionInfo->total_dof_end();
            for (auto it = it_beg; it != it_end; ++it) {
                if (*it<dcgp.mSolution.rows())
                    sol[*it] = dcgp.mSolution(std::distance(it_beg, it), 0);
            }
        }
        
        MSL_FP_TYPE error = spaceU->errorSemiH1_squared(Vector(sol),
                                                        *func,
                                                        i_order,
                                                        dcgp.mPartitionInfo.get());
        return sqrt(error);

    }));
    
    mErrorFunctions.insert(std::make_pair("L2_dual",
                                          [](Problem &p, int i_order) -> MSL_FP_TYPE
    {
      
        DiffusionCGProblem& dcgp = static_cast<DiffusionCGProblem&>(p);

        std::shared_ptr<FunctionData> func = std::dynamic_pointer_cast<FunctionData>(dcgp.dual_u);
        if (!func) {
            LOG(FATAL) << "Function is missing!";
            exit(1);
        }
        
        std::shared_ptr<StdFiniteElementSpace> spaceU(NULL);
        if (dcgp.mSpaceList.empty()) {
            dcgp.configureSpaces();
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
        else {
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
      
        if (i_order<=0) i_order = 2*dcgp.iPdual_u;
        i_order = std::max(2*(dcgp.iPkappa+dcgp.mPkList[0]-1),i_order);
        
        if (dcgp.mSolution.rows()==0)
            dcgp.solve();
      
        //To print a solution we must account for the fact that the
        //partitioning strategy might have changed the DoF numbering
        //so that their indexes do not correspond to the indexes of the
        //geometric entities (vertices, elements etc), and we must recover
        //such correspondence before printing!
        MSL_VECTOR_FP_TYPE sol(dcgp.mSolution.rows());
        if (dcgp.mPartitionInfo == NULL)
          sol = dcgp.mSolution.block(0, 0, dcgp.mSolution.rows(), 1);
        else {
          auto it_beg = dcgp.mPartitionInfo->total_dof_begin();
          auto it_end = dcgp.mPartitionInfo->total_dof_end();
          for (auto it = it_beg; it != it_end; ++it) {
              if (*it<dcgp.mSolution.rows())
                  sol[*it] = dcgp.mSolution(std::distance(it_beg, it), 0);
          }
        }
        
        Vector v(sol);
      
        // Creating 'grad phi' block for P^iPk functions
        OneFunctionBlock du(-v,
                            spaceU.get(),
                            spaceU->getTensorType(approx_space::gradU),
                            dcgp.mPartitionInfo.get());
        
        du.setVarType(approx_space::gradU);
        du.create(block::INNER_NODE, i_order);

        MSL_FP_TYPE error = (((*dcgp.kappa)*du)-*func)*(((*dcgp.kappa)*du)-*func);
        
        return sqrt(error);
      
    }));
    
    mErrorFunctions.insert(std::make_pair("semiHdiv_dual",
                                          [](Problem &p, int i_order) -> MSL_FP_TYPE
    {
      
        DiffusionCGProblem& dcgp = static_cast<DiffusionCGProblem&>(p);

        std::shared_ptr<FunctionData> func = std::dynamic_pointer_cast<FunctionData>(dcgp.f);
        if (!func) {
            LOG(FATAL) << "Function is missing!";
            exit(1);
        }

        std::shared_ptr<StdFiniteElementSpace> spaceU(NULL);
        if (dcgp.mSpaceList.empty()) {
            dcgp.configureSpaces();
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
        else {
            //the first is the one inserted in configureSpaces()!
            spaceU = std::dynamic_pointer_cast<StdFiniteElementSpace>(*(dcgp.mSpaceList.begin()));
        }
      
        if (i_order<=0) i_order = 2*dcgp.iPdual_u;
        int kappa_Rk = std::max(2*(dcgp.iPkappa+dcgp.mPkList[0]-2),i_order);

        if (dcgp.mSolution.rows()==0)
            dcgp.solve();
      
        //To print a solution we must account for the fact that the
        //partitioning strategy might have changed the DoF numbering
        //so that their indexes do not correspond to the indexes of the
        //geometric entities (vertices, elements etc), and we must recover
        //such correspondence before printing!
        MSL_VECTOR_FP_TYPE sol(dcgp.mSolution.rows());
        if (dcgp.mPartitionInfo == NULL)
            sol = dcgp.mSolution.block(0, 0, dcgp.mSolution.rows(), 1);
        else {
            auto it_beg = dcgp.mPartitionInfo->total_dof_begin();
            auto it_end = dcgp.mPartitionInfo->total_dof_end();
            for (auto it = it_beg; it != it_end; ++it) {
                if (*it<dcgp.mSolution.rows())
                    sol[*it] = dcgp.mSolution(std::distance(it_beg, it), 0);
          }
        }
        
        /* FJA: previous simplification of Div.(kappa.GradU) into kappa.LaplaceU
         * is not working with kappa non scalar :O(
         * noting that (Div.kappa).GradU + A:GradGradU, and assuming A independant of x,y ?
         * now computing second term with tr(kappa.hessianU) */
        // Creating 'hessian of u' block for P^iPk functions
        OneFunctionBlock hessianU(Vector(sol),
                                  spaceU.get(),
                                  spaceU->getTensorType(approx_space::hessianU),
                                  dcgp.mPartitionInfo.get());
      
        hessianU.setVarType(approx_space::hessianU);
        hessianU.create(block::INNER_NODE, std::max(2*dcgp.iPf, kappa_Rk));

        MSL_FP_TYPE error = (tr((*dcgp.kappa)*hessianU) + *func)*(tr((*dcgp.kappa)*hessianU) + *func);

        return error;
      
    }));
    
}

////----------------------------------------------------------------------------
////This is where the data preparation process must be programmed...
////----------------------------------------------------------------------------
void DiffusionCGProblem::readDataFilesImpl(const VectorOfStrings &df) {

    mDataFiles = df;

    TensorType varType = TensorType(SpaceOrderTrait<DiffusionCGProblem>::space_order,
                                    mMesh.getMeshDim());
    TensorType dvarType = FiniteElementSpace::getTensorType(
            varType, approx_space::gradU, mMesh.getMeshDim());

    std::map< std::string , std::pair<std::shared_ptr<Data>*, const TensorType*> > vars;

    //The tensor type of this coefficient is *NOT* definitive, i.e., such type
    //may be changed
    vars["kappa"] = std::make_pair(&kappa,&UNDEFINED_TYPE);

    //The tensor types of these variables are definitive, i.e., there are no
    //other options for their tensor types
    vars["f"] = std::make_pair(&f,&varType);
    vars["u"] = std::make_pair(&u,&varType);
    vars["du"] = std::make_pair(&du,&dvarType);
    vars["q"] = std::make_pair(&dual_u,&dvarType);

    std::map< std::string , int* > iOrders;
    iOrders["kappa_iOrder"] = &iPkappa;
    iOrders["f_iOrder"] = &iPf;
    iOrders["u_iOrder"] = &iPu;
    iOrders["du_iOrder"] = &iPdu;
    iOrders["q_iOrder"] = &iPdual_u;

    read_luaTable<DiffusionDataTypeList>(mPathData + "/" + df[0], vars, iOrders);

}

////----------------------------------------------------------------------------
////This is where the space construction must be programmed...
////----------------------------------------------------------------------------
void DiffusionCGProblem::configureSpacesImpl() {

    //Creating function spaces
    LOG(DEBUG) << "Creating local space of Lagrange...";
    //TODO[Tadeu]: use of getRefElemInfo() not generic enough
    localSpaceU.setElemInfo(mMesh.getMapOfElemInfoTypes().begin()->second);
    localSpaceU.setOrder(mPkList[0]);
    localSpaceU.defineDoFInfo();
    LOG(DEBUG) << "Done creating local space of Lagrange!";

    mSpaceList.clear();

    LOG(DEBUG) << "Creating space of Lagrange...";
    auto spaceU = std::make_shared<StdFiniteElementSpace>();
    mSpaceList.push_back(spaceU);
    spaceU->create(mMesh, localSpaceU);
    LOG(DEBUG) << "done!";

    cleanUpInactiveLabels();

    spaceU->setEssentialBC(mDirichletBC);

    genKernelSize();

    if(mDimKernel>0) {
        //Creating function spaces
        LOG(DEBUG) << "creating local space of Lagrange multipliers...";
        
        //TODO[Tadeu]: use of getRefElemInfo() not generic enough
        localSpaceU0 = PiecewiseLagrangeSpace(
                    0,
                    mMesh.getMapOfElemInfoTypes().begin()->second);
        mSpaceList.push_back(std::make_shared<StdFiniteElementSpace>(mMesh, localSpaceU0));
        
        LOG(DEBUG) << "done!";
    }

}

////----------------------------------------------------------------------------
////This is where the weak form for the local problem must be programmed...
////----------------------------------------------------------------------------
void DiffusionCGProblem::buildVariationalFormImpl() {

    int f_Rk = mPkList[0] +iPf;
    int Kappa_Rk = std::max(iPkappa+2*(mPkList[0]-1),0);

    StdFiniteElementSpace &spaceU = static_cast<StdFiniteElementSpace&>(*(mSpaceList[0].get()));

    // Creating 'phi' block for P^iPk functions
	// This creation works for the case of f being approximated
	// by a polynom of the same degree iPk.
	// If not, the 2th parameter needs to be defined to account
	// for the necessary quadrature size.
    GalerkinLagrangeBlock phi(
                &spaceU,
                block::INNER_NODE,
                f_Rk,
                approx_space::U,
                mPartitionInfo.get());

    // Creating 'grad phi' blocks for P^iPk functions
    GalerkinLagrangeBlock dphi(
                &spaceU,
                block::INNER_NODE,
                Kappa_Rk,
                approx_space::gradU,
                mPartitionInfo.get());

    if (!mLHS) mLHS = std::make_shared<LocalContribSet>();
    if (!mRHS) mRHS = std::make_shared<LocalContribSet>();
    if (mLHS==NULL || mRHS==NULL) {
        LOG(FATAL)
            << "Could not allocate the LocalContribSets. ABORT!";
        exit(1);
    }
    
    *mLHS = ((*kappa)*dphi)*dphi;

    *mRHS = DataBlock(*f)*phi;
    
    if (mDimKernel>0) {

        StdFiniteElementSpace &spaceU0 = static_cast<StdFiniteElementSpace&>(*(mSpaceList[1].get()));

        // Creating 'grad phi' blocks for P^iPk functions
        GalerkinLagrangeBlock multiplier(
                    &spaceU0,
                    block::INNER_NODE,
                    f_Rk,
                    approx_space::U,
                    mPartitionInfo.get());

        *mLHS += multiplier*phi;
        
        *mLHS += phi*multiplier;

    }

}

////----------------------------------------------------------------------------
////This is where the problem solution process must be programmed...
////----------------------------------------------------------------------------
void DiffusionCGProblem::solveImpl(bool aCleanLCS,
                                   bool aCleanAXB) {
    
#ifdef USE_MPI
    if (mParallelismType == ParallelismType::DISTRIBUTED) {
#ifdef USE_MUMPS
        if (!mAXB)
            mAXB = std::make_shared<MUMPSDistLinearSystem>("", false);
#endif
    }
    else {
        if (!mAXB)
            mAXB = std::make_shared<LocalLinearSystem>("", false);
    }
#else
    if (!mAXB)
        mAXB = std::make_shared<LocalLinearSystem>("", false);
#endif

    bool has_multi_rhs = mNumberOfRHS > 1 || mRHS->getNumTrialSpaces() > 1;
    
    if (has_multi_rhs)
        mAXB->mountAXB(*mLHS, *mRHS);
    else
        mAXB->mountAxb(*mLHS, *mRHS);

    if (aCleanLCS) cleanContribs();
    
    if (has_multi_rhs) {
        mAXB->solveAXB();
        mSolution = std::move(mAXB->getX());
    }
    else {
        mAXB->solveAxb();
        mSolution = std::move(mAXB->getx());
    }
    
    if (aCleanAXB)
        mAXB.reset();

}

////----------------------------------------------------------------------------
////This is where the problem solution process must be programmed...
////----------------------------------------------------------------------------
void DiffusionCGProblem::solveImpl(const MSL_MATRIX_FP_TYPE& aRHS,
                                   bool aCleanLCS,
                                   bool aCleanAXB) {

#ifdef USE_MPI
    if (mParallelismType == ParallelismType::DISTRIBUTED) {
#ifdef USE_MUMPS
        if (!mAXB)
            mAXB = std::make_shared<MUMPSDistLinearSystem>("");
#endif
    }
    else {
        if (!mAXB)
            mAXB = std::make_shared<LocalLinearSystem>("");
    }
#else
    if (!mAXB)
        mAXB = std::make_shared<LocalLinearSystem>("");
#endif

    //Mount system
    MSL_VECTOR_FP_TYPE bPartFromA = mAXB->mountA(*(mLHS.get()));

    if (aCleanLCS) cleanContribs();

    mAXB->solveAXDenseB(aRHS.colwise()-bPartFromA);

    mSolution = std::move(mAXB->getX());

    if (aCleanAXB)
        mAXB.reset();

}
