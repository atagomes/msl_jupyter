-- Choose an epsilon:
epsilon = 1.0      -- very low frequence
--epsilon = 1.0/5.6  -- low frequence
--epsilon = 1.0/44.8 -- high frequence
------------------------------------------

-- Definitions
pi = math.pi
sin = math.sin
cos = math.cos

-- Coefficients
alpha1 = 0.25
beta1  = 0.999
gamma1 = 2*pi/epsilon
gamma2 = 5.2*pi

-- Auxiliary functions
function kappax(x)
    return 1. / (alpha1-beta1*(x-x*x)*sin(gamma1*x))
end
function kappay(y)
    return 1. / (alpha1-beta1*(y-y*y)*cos(gamma2*y))
end
function G(x)
    return alpha1*x +
		(beta1/(gamma1*gamma1))*(2*x-1)*sin(gamma1*x) +
		(beta1/(gamma1*gamma1*gamma1))*(2-gamma1*gamma1*(x*x-x))*cos(gamma1*x)
end

-- Diffusion problem
diffusion = {
    kappa = function(x,y) return kappax(x)*kappay(y) end,
    --
    f     = 0.0,
    --
    u     = function(x,y) return (G(1)-G(x))/(G(1)-G(0)) end,
    --
    du    = function(x,y)
		return {
gamma1^3 * (-alpha1 - beta1*x^2*sin(gamma1*x) + beta1*x*sin(gamma1*x))/(alpha1*gamma1^3 + beta1*gamma1*sin(gamma1) + 2*beta1*cos(gamma1) - 2*beta1),
0
		}
	end,
    --
    q     = function(x,y)
		return {
gamma1^3*(alpha1 + beta1*x^2*sin(gamma1*x) - beta1*x*sin(gamma1*x))/((alpha1 + beta1*x*(x - 1)*sin(gamma1*x))*(alpha1 + beta1*y*(y - 1)*cos(gamma2*y))*(alpha1*gamma1^3 + beta1*gamma1*sin(gamma1) + 2*beta1*(cos(gamma1) - 1))),
0
		}
	end,
    --
    f_iOrder = 12,
    u_iOrder = 12,
    du_iOrder = 12,
    q_iOrder = 12,
    --
    g = {
        ["2"] = 0,
        ["4"] = 1,
        ["5"] = 1  --needed because of a bizarre behavior of Triangle mesh generator
    },
    --
    h = {
        ["1"] = 0,
        ["3"] = 0
    }
}
