#include <mpi.h>

#include <memory>
#include <string>
#include <valarray>
#include <vector>

#include "Easylogging++/include/easylogging++.h"

#include "problem_cgdiffusion.h"
#include "problem_mhmcgdecorator.h"
#include "Facade/problem_mhmglobal.h"
#include "Facade/problem_mhmlocal.h"
#include "Utils/utils.h"

//INITIALIZE_EASYLOGGINGPP

int main(int argc, char *argv[]) {

    //! [Configuring MPI]
    int mpi_rank, required = MPI_THREAD_MULTIPLE, provided = -1;

    MPI_Init_thread(&argc, &argv, required, &provided);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    //! [Configuring MPI]

    {
    MSL_configure_easyloggingpp();

    std::string path_conf(argv[1]);

    //! [Configuring global problem]
    MHMGlobalProblem<DiffusionCGProblem, CGMHMDecorator<DiffusionCGProblem>> P0;
    P0.readDataFiles({path_conf});
    P0.setSymmetry(true);

    if (mpi_rank==0) {
      P0.printParams();
      P0.printData();
    }

    P0.doIterativeSplit(NULL, NULL);
    P0.configureSpaces();
    std::shared_ptr<PartitionInfo> pi = P0.getPartitionInfoPtr();
    P0.buildVariationalForm();
    //! [Configuring global problem]

    //! [Configuring and solving local problems]
    if (mpi_rank==0) LOG(INFO) << "Starting solving local problems";
    std::vector<
        MHMLocalProblem<DiffusionCGProblem, CGMHMDecorator<DiffusionCGProblem>>
    >
    SPs(pi->getNumElems());
    
    PartitionInfo::elem_iterator it = pi->elem_begin();
#pragma omp parallel for
    for (int L=0; L<pi->getNumElems(); ++L) {
        P0.sendAlreadyPrepared(*(it+L));
        std::vector<LocalContrib*> contrib_LHSs(P0.getLHS()->getNumContribSets());
        std::vector<LocalContrib*> contrib_RHSs(P0.getRHS()->getNumContribSets());
        for (int i=0; i<contrib_LHSs.size(); ++i)
            contrib_LHSs[i] = &(P0.getLHS()->getContribSet(i)[L]);
        for (int i=0; i<contrib_RHSs.size(); ++i)
            contrib_RHSs[i] = &(P0.getRHS()->getContribSet(i)[L]);
        
        SPs[L].setPathData(P0.buildLocalProblemConfigDir(*(it+L)));
        SPs[L].readDataFiles({"config_mhm"});
        
        SPs[L].configureSpaces();
        SPs[L].buildVariationalForm();
        
        SPs[L].solve(contrib_LHSs, contrib_RHSs);
    }
    if (mpi_rank==0) LOG(INFO) << "Finished solving local problems";
    
    //...................... MPI BARRIER ..............................
    MPI_Barrier(MPI_COMM_WORLD);

    //......................... GLOBAL PROBLEM SOLVING .........................
    if (mpi_rank==0) LOG(INFO) << "Starting solving global problem";
    LocalContribSet unmount_sol;
    P0.solve(unmount_sol);
    if (mpi_rank==0) LOG(INFO) << "Finished solving global problem";
    
    //................................ OUTPUT SETUP ............................
    if (mpi_rank==0) LOG(INFO) << "Starting computing final solution";
#pragma omp parallel for
    for (int L=0; L<pi->getNumElems(); ++L) {
        std::vector<LocalContrib*> sol_contribs(unmount_sol.getNumContribSets());
        for (int i=0; i<sol_contribs.size(); ++i)
            sol_contribs[i] = &(unmount_sol.getContribSet(i)[L]);
        
        SPs[L].computeSolution(sol_contribs);
    }
    if (mpi_rank==0) LOG(INFO) << "Finished computing final solution";
    
    //.......................... ERROR COMPUTATION .............................
    std::valarray<MSL_FP_TYPE> local_errorsL2(0.0, pi->getNumElems());
    std::valarray<MSL_FP_TYPE> local_errorsH1(0.0, pi->getNumElems());
    
    if (mpi_rank==0) LOG(INFO) << "Starting computing errors";
#pragma omp parallel for
    for (int L=0; L<pi->getNumElems(); ++L) {
        local_errorsL2[L] = pow(SPs[L].computeError("L2"), 2);
        local_errorsH1[L] = local_errorsL2[L] +
                            pow(SPs[L].computeError("semiH1"), 2);
    }
    
    MSL_FP_TYPE local_sum_errorsL2 = local_errorsL2.sum();
    MSL_FP_TYPE local_sum_errorsH1 = local_errorsH1.sum();
    
    MSL_FP_TYPE global_sum_errorsL2 = 0;
    MSL_FP_TYPE global_sum_errorsH1 = 0;

    MPI_Allreduce(&local_sum_errorsL2, &global_sum_errorsL2, 1,
                  MSL_MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&local_sum_errorsH1, &global_sum_errorsH1, 1,
                  MSL_MPI_FP_TYPE, MPI_SUM, MPI_COMM_WORLD);

    global_sum_errorsL2 = sqrt(global_sum_errorsL2);
    global_sum_errorsH1 = sqrt(global_sum_errorsH1);

    if (mpi_rank==0) LOG(INFO) << "Finished computing errors";

    if (mpi_rank==0) {
      LOG(INFO) << "L2-norm error: " << global_sum_errorsL2;
      LOG(INFO) << "H1-norm error: " << global_sum_errorsH1;
    }
    //! [Computing solution and approximation error]
    }

    //! [Releasing MPI]
    MPI_Finalize();
    //! [Releasing MPI]

    return 0;
    
}

