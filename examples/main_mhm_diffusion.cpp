#include <memory>
#include <string>
#include <valarray>
#include <vector>

#include "Easylogging++/include/easylogging++.h"

#include "problem_cgdiffusion.h"
#include "problem_mhmcgdecorator.h"
#include "Facade/problem_mhmglobal.h"
#include "Facade/problem_mhmlocal.h"
#include "Utils/utils.h"

//INITIALIZE_EASYLOGGINGPP

int main(int argc, char *argv[]) {

    MSL_configure_easyloggingpp();
    
    std::string path_conf(argv[1]);

    //! [Configuring global problem]
    std::string splitlog;
    std::vector<std::string> branches;
    
    MHMGlobalProblem<DiffusionCGProblem, CGMHMDecorator<DiffusionCGProblem>> P0;
    P0.readDataFiles({path_conf});
    P0.setSymmetry(true); 

    P0.printParams();
    P0.printData();

    splitlog = P0.splitToDisk(branches);
    //! [Configuring global problem]

    //! [Configuring and solving local problems]
    LOG(INFO) << "Starting solving local problems";
    std::vector<
        MHMLocalProblem<DiffusionCGProblem, CGMHMDecorator<DiffusionCGProblem>>
    >
    SPs(branches.size());
    
#pragma omp parallel for
    for (int L=0; L<branches.size(); ++L) {
        std::vector<LocalContrib*> contrib_LHSs(P0.getLHS()->getNumContribSets());
        std::vector<LocalContrib*> contrib_RHSs(P0.getRHS()->getNumContribSets());
        for (int i=0; i<contrib_LHSs.size(); ++i)
            contrib_LHSs[i] = &(P0.getLHS()->getContribSet(i)[L]);
        for (int i=0; i<contrib_RHSs.size(); ++i)
            contrib_RHSs[i] = &(P0.getRHS()->getContribSet(i)[L]);

        SPs[L].setPathData(branches[L]);
        SPs[L].readDataFiles({"config_mhm"});
        
        SPs[L].configureSpaces();
        SPs[L].buildVariationalForm();
        
        SPs[L].solve(contrib_LHSs, contrib_RHSs);
        
        SPs[L].printMeshWithHeaders(branches[L], "micromesh");
    }
    LOG(INFO) << "Finished solving local problems";
    //! [Configuring and solving local problems]

    //! [Solving global problem]
    LOG(INFO) << "Starting solving global problem";
    LocalContribSet unmount_sol;
    P0.solve(unmount_sol);
    LOG(INFO) << "Finished solving global problem";
    //! [Solving global problem]

    //! [Computing solution and approximation error]
    std::valarray<MSL_FP_TYPE> local_errorsL2(0.0, branches.size());
    std::valarray<MSL_FP_TYPE> local_errorsH1(0.0, branches.size());
    std::valarray<MSL_FP_TYPE> local_errorsL2_dual(0.0, branches.size());
    std::valarray<MSL_FP_TYPE> local_errorsHdiv_dual(0.0, branches.size());

    LOG(INFO) << "Starting computing final solution";
#pragma omp parallel for
    for (int L=0; L<branches.size(); ++L) {
        std::vector<LocalContrib*> sol_contribs(unmount_sol.getNumContribSets());
        for (int i=0; i<sol_contribs.size(); ++i)
            sol_contribs[i] = &(unmount_sol.getContribSet(i)[L]);
        
        SPs[L].computeSolution(sol_contribs);
        SPs[L].appendResultsToMesh();
        
        local_errorsL2[L] = pow(SPs[L].computeError("L2"), 2);
        local_errorsH1[L] = local_errorsL2[L] +
        pow(SPs[L].computeError("semiH1"), 2);
        local_errorsL2_dual[L] = pow(SPs[L].computeError("L2_dual"), 2);
        local_errorsHdiv_dual[L] = local_errorsL2_dual[L] +
                                        SPs[L].computeError("semiHdiv_dual");
    }
    LOG(INFO) << "Finished computing final solution";
    
    MSL_FP_TYPE global_sum_errorsL2 = local_errorsL2.sum();
    MSL_FP_TYPE global_sum_errorsH1 = local_errorsH1.sum();
    MSL_FP_TYPE global_sum_errorsL2_dual = local_errorsL2_dual.sum();
    MSL_FP_TYPE global_sum_errorsHdiv_dual = local_errorsHdiv_dual.sum();
    
    global_sum_errorsL2 = sqrt(global_sum_errorsL2);
    global_sum_errorsH1 = sqrt(global_sum_errorsH1);
    global_sum_errorsL2_dual = sqrt(global_sum_errorsL2_dual);
    global_sum_errorsHdiv_dual = sqrt(global_sum_errorsHdiv_dual);

    LOG(INFO) << "L2-norm error: " << global_sum_errorsL2;
    LOG(INFO) << "H1-norm error: " << global_sum_errorsH1;
    LOG(INFO) << "L2_dual-norm error: " << global_sum_errorsL2_dual;
    LOG(INFO) << "Hdiv_dual-norm error: " << global_sum_errorsHdiv_dual;
    //! [Computing solution and approximation error]

    return 0;
    
}

