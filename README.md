MSL Jupyter: Experimenting with the MSL libraries in Jupyter!
===

This project allows students, practitioners and researchers to explore the set of libraries called MSL, developed by LNCC, Brazil. This project offers a Jupyter notebook application contained within a Docker image, a set of source code and data files that may be uploaded to the Jupyter notebook for running MSL examples.

The MSL comprises 3 main libraries, developed in C++ and Lua, that allow the implementation of parallel numerical simulators based on Finite-Element Methods (FEM):

- MSL_Core: classes implementing key concepts present in most FEM-based simulators;
- MSL_CG: classes implementing key concepts present in Continuous Galerkin-based simulators;
- MSL_MHM: classes implementing key concepts present in the Multiscale Hybrid Mixed-based simulators.

Installing and running the Docker container
---

To experiment with MSL by using our Jupyter notebook application, you must have a machine with Docker (https://www.docker.com/get-started) installed. (If your user account does not have superuser privileges, you may need to run the `docker` commands below with `sudo`.)

The first step is to download the Docker image from the container registry of this project:
```
docker pull registry.gitlab.com/atagomes/msl_jupyter/msljupyter:latest
```

The second step is to instantiate a container from the downloaded Docker image:
```
docker run --rm -d -p 8888:8888/tcp registry.gitlab.com/atagomes/msl_jupyter/msljupyter:latest
```

The last command above outputs a hash that is the container id. You must copy it and use it to replace the `container_id` string in the next command to be issued:
```
docker logs --tail 1000 -f  container_id
```

The output from the last command above should be something like the following:
```
[I 18:02:19.948 NotebookApp] Writing notebook server cookie secret to /home/msl_jupyter/.local/share/jupyter/runtime/notebook_cookie_secret
[I 18:02:20.153 NotebookApp] Serving notebooks from local directory: /home/msl_jupyter/notebooks
[I 18:02:20.153 NotebookApp] Jupyter Notebook 6.2.0 is running at:
[I 18:02:20.153 NotebookApp] http://f9879a994bc8:8888/?token=b359500e8c8ccffb3c974f751c3cf5d329c7ec685dc3c0bc
[I 18:02:20.153 NotebookApp]  or http://127.0.0.1:8888/?token=b359500e8c8ccffb3c974f751c3cf5d329c7ec685dc3c0bc
[I 18:02:20.153 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 18:02:20.156 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///home/msl_jupyter/.local/share/jupyter/runtime/nbserver-1-open.html
    Or copy and paste one of these URLs:
        http://f9879a994bc8:8888/?token=b359500e8c8ccffb3c974f751c3cf5d329c7ec685dc3c0bc
     or http://127.0.0.1:8888/?token=b359500e8c8ccffb3c974f751c3cf5d329c7ec685dc3c0bc
```

Copy the last URL that appears in the output from the `docker logs` command (the URL that starts with `http://127.0.0.1:8080/?token=`) and paste the URL in the address bar of your favorite's web browser. 

That's it! You're read to go!!

To check if you have a container already running, issue:
```
docker ps -a
```

The output from the last command above should be something like the following:
```
CONTAINER ID   IMAGE                          COMMAND                  CREATED         STATUS                     PORTS     NAMES
e40485891ae9   registry.gitlab.com/ipes/...   "jupyter-notebook --…"   12 months ago   Exited (0) 4 seconds ago             adoring_faraday
```

And to start again a container you've stopped before (its STATUS will appear as `Exited` in the `docker ps -a` command), issue:
```
docker start -ai container_id
```



